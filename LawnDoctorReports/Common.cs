﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using DataAccess;
using System.IO;
using System.Data.Objects.DataClasses;
using System.Reflection;
using System.Text;
using System.Windows.Forms;


namespace LawnDoctorReports
{
    public class Common
    {

        public static string ToCsv<T>(string separator, IEnumerable<T> objectList)
        {
            Type t = typeof(T);
            PropertyInfo[] properties = t.GetProperties();

            string header = String.Join(separator, properties.Select(f => f.Name).ToArray());

            StringBuilder csvdata = new StringBuilder();
            csvdata.AppendLine(header);

            foreach (var curObject in objectList)
                csvdata.AppendLine(ToCsvProperties(separator, properties, curObject));

            return csvdata.ToString();
        }

        public static string ToCsvProperties(string separator, PropertyInfo[] properties, object curObject)
        {
            StringBuilder csvLine = new StringBuilder();
            foreach (var property in properties)
            {
                if (csvLine.Length > 0)
                {
                    csvLine.Append(separator);
                }

                var propValue = property.GetValue(curObject, null);
                string appendValue = propValue == null ? string.Empty : propValue.ToString();
                csvLine.Append(string.Format("\"{0}\"", propValue));
            }

            return csvLine.ToString();
        }

        //public static string ToCsv<T>(string separator, IEnumerable<T> objectlist)
        //{
        //    Type t = typeof(T);
        //    FieldInfo[] fields = t.GetFields();

        //    string header = String.Join(separator, fields.Select(f => f.Name).ToArray());

        //    StringBuilder csvdata = new StringBuilder();
        //    csvdata.AppendLine(header);

        //    foreach (var o in objectlist)
        //        csvdata.AppendLine(ToCsvFields(separator, fields, o));

        //    return csvdata.ToString();
        //}

        //public static string ToCsvFields(string separator, FieldInfo[] fields, object o)
        //{
        //    StringBuilder linie = new StringBuilder();

        //    foreach (var f in fields)
        //    {
        //        if (linie.Length > 0)
        //            linie.Append(separator);

        //        var x = f.GetValue(o);

        //        if (x != null)
        //            linie.Append(x.ToString());
        //    }

        //    return linie.ToString();
        //}

        public static string CreatePath(string dist_no)
        {
            string mypath = "";

            mypath = mypath + "\\Kubota Tractor Corporation-840220\\OnlineJobs\\" + DateTime.Now.Year.ToString();

            //switch (dist_no)
            //{
            //    case "2":
            //        mypath = mypath + "\\Blalock Machinery & Equipment-838454\\OnlineJobs\\" + DateTime.Now.Year.ToString();
            //        break;
            //    case "3":
            //        mypath = mypath + "\\Kubota Tractor Corporation-840220\\OnlineJobs\\" + DateTime.Now.Year.ToString();
            //        break;
            //    case "4":
            //        mypath = mypath + "\\Power Equipment Distribution-522846\\OnlineJobs\\" + DateTime.Now.Year.ToString();
            //        break;
            //    case "5":
            //        mypath = mypath + "\\Horizon Distributers-838455\\OnlineJobs\\" + DateTime.Now.Year.ToString();
            //        break;
            //    case "6":
            //        mypath = mypath + "\\Lepco-838456\\OnlineJobs\\" + DateTime.Now.Year.ToString();
            //        break;
            //    case "7":
            //        mypath = mypath + "\\Lepco-838456\\OnlineJobs\\" + DateTime.Now.Year.ToString();
            //        break;
            //    case "8":
            //        mypath = mypath + "\\Power Trim-838459\\OnlineJobs\\" + DateTime.Now.Year.ToString();
            //        break;
            //    case "9":
            //        mypath = mypath + "\\RW Distributors-838460\\OnlineJobs\\" + DateTime.Now.Year.ToString();
            //        break;
            //    case "10":
            //        mypath = mypath + "\\Roberts Supply-522100\\OnlineJobs\\" + DateTime.Now.Year.ToString();
            //        break;
            //    case "11":
            //        mypath = mypath + "\\South Shore\\OnlineJobs\\" + DateTime.Now.Year.ToString();
            //        break;
            //    case "12":
            //        mypath = mypath + "\\Echo-846528\\OnlineJobs\\" + DateTime.Now.Year.ToString();
            //        break;
            //    case "13":
            //        mypath = mypath + "\\Hawthorne\\OnlineJobs\\" + DateTime.Now.Year.ToString();
            //        break;
            //    default:
            //        mypath = "";
            //        break;

            //}

            return mypath;
        }

        public static string LotSizeToMowTime(Decimal lotSize)
        {
            Decimal acre = 43560; //in square feet
            // Acre Per Hour Formula
            Decimal aph = 6m * 60m / 124m;
            Decimal timePerAcre = 60m / aph;
            Decimal lotAcres = lotSize / acre;
            Decimal timePerLot = timePerAcre * lotAcres; //Time in Minutes
            //Round up to next Quater Hour
            Decimal rndTime = (timePerLot - (timePerLot % 15m)) + 15m;

            return String.Format("{0:#####}", rndTime);
        }


        //public static void passgenerator()
        //{
        //    MFGEntities db = new MFGEntities();
        //    string tick = DateTime.Now.Ticks.ToString();
        //    Random rnd = new Random(Convert.ToInt32(tick.Substring(tick.Length - 8, 8)));

        //    foreach (var pass in db.employees.Where(w => w.password == null))
        //    {

        //        string newPass = OcEncode(rnd.Next(0, 9999999));
        //        pass.password = newPass;
        //        pass.position = newPass;

        //    }
        //    db.SaveChanges();
        //}

        public static string OcEncode(Int64 n)
        {
            string code = "";
            int checkSum = 0;
            string[] alphaBase = { "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "2", "3", "4", "5", "6", "7", "8", "9" };
            for (int i = 4; i >= 0; i += -1)
            {
                int newN = Convert.ToInt32(Math.Floor(n / Math.Pow(31, i)));
                n = n - Convert.ToInt64(newN * Math.Pow(31, i));
                code += alphaBase[newN];
                checkSum += newN;
            }
            return code;

        }

        //public static void GetPClist()
        //{
        //    DataSet dsPClist = new DataSet();
            
        //    SqlConnection connection = new SqlConnection(DataAccess.DataAccess.connStr);
        //    connection.Open();

        //    SqlCommand command = connection.CreateCommand();
        //    command.CommandType = CommandType.StoredProcedure;
        //    command.CommandText = "dbo.mfg_letters_pc";

        //    SqlDataAdapter adapter = new SqlDataAdapter(command);

        //    adapter.Fill(dsPClist);

        //    string sThemeName = "";
        //    if (dsPClist.Tables[0].Rows.Count > 0)
        //    {
        //        sThemeName = dsPClist.Tables[0].Rows[0]["lettername"].ToString().Trim();
        //    }

        //    if (sThemeName == "")
        //    {
        //        sThemeName = "Default";
        //    }
            
        //}
    }
    public class ConditionSummaryObject
    {
        public string market { get; set; }
        public int conditionsUsed { get; set; }
        public int sellConditionsUsed { get; set; }
        public int conditionsUsedYTD { get; set; }
        public int sellConditionsUsedYTD { get; set; }
        public int activeCustomers { get; set; }
    }
}