﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using CommonTools;
using DataAccess;
using System.Data;
using System.Data.OleDb;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Data.Common;
using System.Data.Objects;
using System.Data.Entity.Infrastructure;
using NotificationEmail;
using System.IO;
using Ionic.Zip;
using System.Data.Entity.Validation;
using System.Diagnostics;


namespace LawnDoctorReports
{
    class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// 
        //[STAThread]

        public static DataTable AllFranchises;
        public static DataTable datafees;
        public static List<curcredcodecls> curCredCode;
        public static bool bothreports;
        public static int rpt_weeknum;
        public static DateTime fromdate;
        public static DateTime todate;
        public static int rpt_year;
        public static bool chargeoutofarea;
        public static DateTime lastsync;
        public static string mypathstart;
        public static decimal royalty_in = 10;
        public static decimal royalty_out = 25;
        public static string sqlcmd;
        public static bool isweekone = false;
        public static List<byte> SQLDBIDtoProcess_WEBS = new List<byte>();
        public static List<byte> SQLDBIDtoProcess_WEBS_To_Process = new List<byte>();
        public static string tdate;
        public static string fdate;
        public static string databaseName;
        public static string Franchise_Account_Prefix;
        public static string Company_Of_String;
        public static string mailSubjectPrefix;

        //public static servicefeecls servicefee = new servicefeecls();

        static void Main()
        {
            databaseName = LawnDoctorReports.Properties.Settings.Default.LawnDoctorTransactionDBConnection;
            databaseName = databaseName.ToString().Trim().ToUpper();
            Franchise_Account_Prefix = (databaseName.IndexOf("LAWNDOCTORTRANSACTIONDB") > 0) ? "C" : "M";
            mailSubjectPrefix = (databaseName.IndexOf("LAWNDOCTORTRANSACTIONDB") > 0) ? "" : "MosquitoHunters - ";
            Company_Of_String = (databaseName.IndexOf("LAWNDOCTORTRANSACTIONDB") > 0) ? "lawn doctor of" : "Mosquito Hunters of";

            //Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            NotificationEmail.NotificationEmail.ProgressNotification("started - Get current day of week"); 
            int whatIsToday = (int)DateTime.Today.DayOfWeek;

            NotificationEmail.NotificationEmail.ProgressNotification("Create db connection");
            LawnDoctorTransactionDBEntities db = new LawnDoctorTransactionDBEntities();
            var adapter = (IObjectContextAdapter)db;
            var dbobjectContext = adapter.ObjectContext;
            NotificationEmail.NotificationEmail.ProgressNotification("Fill week number table");
            fillweeknumbertable();

            NotificationEmail.NotificationEmail.ProgressNotification("Get week number");
            int week_id = getweeknum();
            //week_id = 327;

            NotificationEmail.NotificationEmail.ProgressNotification("First value from dashld week numbers");
            rpt_weeknum = db.Dash_LD_WeekNumbers.Where(w => w.week_id == week_id).Select(s => s.weeknumber).First();
            if (rpt_weeknum == 1)
            {
                isweekone = true;
            }
            NotificationEmail.NotificationEmail.ProgressNotification("From DAte");
            fromdate = Convert.ToDateTime(db.Dash_LD_WeekNumbers.Where(w => w.week_id == week_id).Select(s => s.fromdate).First());
            NotificationEmail.NotificationEmail.ProgressNotification("From date 2");
            fromdate = fromdate.Date;//getFdatefromweeknum(weekno);
            NotificationEmail.NotificationEmail.ProgressNotification("toidate");
            todate = Convert.ToDateTime(db.Dash_LD_WeekNumbers.Where(w => w.week_id == week_id).Select(s => s.todate).First());
            todate = todate.Date;
            tdate = todate.ToString("MM/dd/yyyy").Trim();
            fdate = fromdate.ToString("MM/dd/yyyy").Trim();
            CreateConditionCodeSummaryCSV();
            
            NotificationEmail.NotificationEmail.ProgressNotification("Get year from the week id");
            rpt_year = db.Dash_LD_WeekNumbers.Where(w => w.week_id == week_id).Select(s => s.year).First();

            NotificationEmail.NotificationEmail.ProgressNotification("Setup path");
            if (databaseName.IndexOf("LAWNDOCTORTRANSACTIONDB") > 0)
                mypathstart = "\\\\Server7\\Public\\Customer Data\\LawnDoctor\\Weekly Reports\\" + rpt_year.ToString().Trim();
            else
                mypathstart = "\\\\Server7\\Public\\Customer Data\\MosquitoHunters\\Weekly Reports\\" + rpt_year.ToString().Trim();

            //if (whatIsToday == 4)  // Run wednesday to create weekly Lawn Doctor report csv files  ... values 0 is Sunday and Saturday is 6
            //{

            //}
            NotificationEmail.NotificationEmail.ProgressNotification("Start deleting of cash compliance");
            dbobjectContext.ExecuteStoreCommand("Delete from Dash_LD_CashCompliance where [weekenddate] = {0}",
               new object[] { todate });

            // delete existing data in dash_ld_servicefee table for this sqldbid,comp_id, weekenddate
            NotificationEmail.NotificationEmail.ProgressNotification("Start deleting of ServiceFee");
            dbobjectContext.ExecuteStoreCommand("Delete from dash_LD_serviceFee where [weekenddate] = {0}",
               new object[] { todate });

            NotificationEmail.NotificationEmail.ProgressNotification("Start deleting of ServiceFeeCorp");
            dbobjectContext.ExecuteStoreCommand("Delete from dash_LD_serviceFeeCorp where [weekenddate] = {0}",
                new object[] { todate });

            NotificationEmail.NotificationEmail.ProgressNotification("Start deleting of ld webs");
            dbobjectContext.ExecuteStoreCommand("Delete from dash_LD_WEBS where [weekenddate] = {0}",
                new object[] { todate });

            NotificationEmail.NotificationEmail.ProgressNotification("LD Franchise table");
            sqlcmd = "Exec LD_FranchiseTable";
            AllFranchises = CommonTools.CommonTools.SQL_GetDT(sqlcmd, LawnDoctorReports.Properties.Settings.Default.LawnDoctorTransactionDBConnection);

            NotificationEmail.NotificationEmail.ProgressNotification("LD_FranchiseTableDups");
            sqlcmd = "Exec LD_FranchiseTableDups";
            DataTable AllFranchisesDups = CommonTools.CommonTools.SQL_GetDT(sqlcmd, LawnDoctorReports.Properties.Settings.Default.LawnDoctorTransactionDBConnection);

            NotificationEmail.NotificationEmail.ProgressNotification("get list of sqldbids where at least one service is missing a budget type.  Will not be included in report");
            // get list of sqldbids where at least one service is missing a budget type.  Will not be included in report
            List<byte> missingbudget = new List<byte>();
            if(string.IsNullOrWhiteSpace(mailSubjectPrefix))
            {
                missingbudget = checkbudgettype();
            }
            

            if (missingbudget.Count != 0)
            {
                string machinename = ""; //Properties.Settings.Default.Server;
                string database = "";  //Properties.Settings.Default.database;
                string tablename = ""; // Properties.Settings.Default.dealertable;
                string subject = "WEBS Will not be included in this week " + rpt_weeknum + " report due to Missing Budget Type on Services";

                List<string> missingbudgetdetails = new List<string>();
                NotificationEmail.NotificationEmail.ProgressNotification("Add missing budget(s)");
                foreach (var item in missingbudget)
                {
                    var details =  db.DashMarketRegions.AsEnumerable().Where(w => w.sqldbid == item).Select(x => x.sqldbname ).FirstOrDefault();

                    //Code for consolidated database, instead of getting the first entry from dashmarketregion
                    if (item == 143)
                        details = "Consolidated database (SQLDBID = 143)";

                    //var details = db.DashMarketRegions.AsEnumerable().Where(w => w.sqldbid == item).Select(x => new { Group = x.sqldbname }).FirstOrDefault();
                    if (details != null)
                    {
                        missingbudgetdetails.Add(details);
                    }
                }

                NotificationEmail.NotificationEmail.MissingInformationNotification(machinename, database, tablename, missingbudgetdetails, subject); // NotificationEmail.NotificationEmail // send emails for 
            }

            // build list of all sqldbids from franchise table with lastsync date before the end of the reporting week, will not incuded in report
            NotificationEmail.NotificationEmail.ProgressNotification("get sqldbids not synced");
            var SQLDBIDnotsynced =  AllFranchises.AsEnumerable().Where(w => w.Field<DateTime>("lastsync") < todate).Select(s => s.Field<byte>("sqldbid")).Distinct().ToList();

            NotificationEmail.NotificationEmail.ProgressNotification("Check syncuptransaction");
            List<int> SQLDBIDbadsync = new List<int>();
            
            if (string.IsNullOrWhiteSpace(mailSubjectPrefix))
            {
                db.CawSyncUpTransactions.Where(w => w.Completed == null && (w.Started >= fromdate && w.Started <= todate)).Select(s => s.SqlDbId).Distinct().ToList();
                if (SQLDBIDnotsynced != null | SQLDBIDbadsync != null)
                {
                    string machinename = ""; //Properties.Settings.Default.Server;
                    string database = "";  //Properties.Settings.Default.database;
                    string tablename = ""; // Properties.Settings.Default.dealertable;
                    string subject = "Service Fee Report will not be included for week " + rpt_weeknum;

                    List<string> SQLDBIDnotsynceddetails = new List<string>();
                    string details;
                    NotificationEmail.NotificationEmail.ProgressNotification("sqldbid's that have not synced after the to date for reporting week");
                    // sqldbid's that have not synced after the to date for reporting week
                    foreach (byte item in SQLDBIDnotsynced)
                    {
                        var onedetail = AllFranchises.AsEnumerable().Where(w => w.Field<byte>("sqldbid") == item).FirstOrDefault();
                        var groupname = db.DashMarketRegions.Where(w => w.sqldbid == item).Select(s => s.sqldbname).FirstOrDefault();

                        //Code for consolidated database, instead of getting the first entry from dashmarketregion
                        if (item == 143)
                            details = "Consolidated database (SQLDBID = 143)";

                        if (groupname == null)
                        {
                            groupname = "Group : " + onedetail.Field<string>("rptgroup").Trim();
                        }

                        string line = groupname + "  Last Sync: " + onedetail.Field<DateTime>("lastsync");
                        line = line + " due to last sync date";
                        details = line;


                        SQLDBIDnotsynceddetails.Add(details);
                    }
                    NotificationEmail.NotificationEmail.ProgressNotification("sqldbid's that have an incomplete sync for reporting week");
                    // sqldbid's that have an incomplete sync for reporting week
                    foreach (byte item in SQLDBIDbadsync)
                    {
                        var onedetail = AllFranchises.AsEnumerable().Where(w => w.Field<byte>("sqldbid") == item).FirstOrDefault();
                        var groupname = db.DashMarketRegions.Where(w => w.sqldbid == item).Select(s => s.sqldbname).FirstOrDefault();
                        //Code for consolidated database, instead of getting the first entry from dashmarketregion
                        if (item == 143)
                            details = "Consolidated database (SQLDBID = 143)";
                        if (groupname == null)
                        {
                            groupname = "Group : " + onedetail.Field<string>("rptgroup").Trim();
                        }

                        string line = groupname;
                        line = line + " due to incomplete sync during reporting week ";
                        details = line;


                        SQLDBIDnotsynceddetails.Add(details);
                    }

                    NotificationEmail.NotificationEmail.ProgressNotification("franchises with duplicate branch number is SA database");
                    //  franchises with duplicate branch number is SA database
                    foreach (DataRow item in AllFranchisesDups.Rows)
                    {
                        byte myid = item.Field<byte>("sqldbid");
                        int mycompid = item.Field<Int32>("comp_id");
                        //var onedetail = AllFranchises.AsEnumerable().Where(w => w.Field<byte>("sqldbid") == myid && w.Field<Int16>("comp_id") == mycompid).FirstOrDefault();
                        var groupname = db.DashMarketRegions.Where(w => w.sqldbid == myid).Select(s => s.sqldbname).FirstOrDefault();
                        //Code for consolidated database, instead of getting the first entry from dashmarketregion
                        if (myid == 143)
                            details = "Consolidated database (SQLDBID = 143)";
                        if (groupname == null)
                        {
                            groupname = "Group : " + item.Field<string>("rptgroup").Trim() + " / Franchise " + item.Field<string>("lawn doctor of").Trim();

                        }
                        else
                        {
                            groupname = groupname + " / Franchise " + item.Field<string>("lawn doctor of").Trim();
                        }
                        string line = groupname + "  Has duplicate Branch Number with same Acct No in Service Assistant - Branch " + item.Field<string>("acctno");
                        details = line;


                        SQLDBIDnotsynceddetails.Add(details);
                    }

                    NotificationEmail.NotificationEmail.MissingInformationNotification(machinename, database, tablename, SQLDBIDnotsynceddetails, subject); // NotificationEmail.NotificationEmail // send emails for 
                }
            }
            // build list of all sqldbids from dashmarket region (not 255 as it is a test for corporate)
            //List<int> allSQLDBID = new List<int>();
            //allSQLDBID = db.DashMarketRegions.Where(w => w.sqldbid != 255).Select(s => s.sqldbid).ToList();

            // build list of all sqldbids from franchise table with lastsync date after the end of the reporting week
            List<byte> SQLDBIDsynced = new List<byte>();
            //SQLDBIDsynced = db.DashMarketRegions.Where(w => w.sqldbid != 255 && w.lastsync >= todate).Select(s => s.sqldbid).ToList();

            if(string.IsNullOrWhiteSpace(mailSubjectPrefix))
            {
                SQLDBIDsynced = AllFranchises.AsEnumerable().Where(w => w.Field<DateTime>("lastsync") >= todate).Select(s => s.Field<byte>("sqldbid")).Distinct().ToList();
            }
            else
            {
                SQLDBIDsynced = AllFranchises.AsEnumerable().Select(s => s.Field<byte>("sqldbid")).Distinct().ToList();
            }
                


            // build a list of sqldbids that have synced at least once since monday of the current calender week and have budget type for all services
            List<byte> SQLDBIDtoProcess = new List<byte>();

            // build a list of sqldbids that have synced at least once since monday of the current calender week and have budget type for all services

            NotificationEmail.NotificationEmail.ProgressNotification("create the list of sqldbids to process");
            //SQLDBIDtoProcess.Add(190);
            //SQLDBIDtoProcess_WEBS.Add(190);
            foreach (var sqldbid in SQLDBIDsynced)
            {
                // see if sqldbid is in missing budget type list, if so do not add to list of sqldbids to be processed
                bool nobudgettype = missingbudget.Contains(sqldbid);
                bool incompletesync = SQLDBIDbadsync.Contains(sqldbid);

                if (nobudgettype == false && incompletesync == false)
                {
                    SQLDBIDtoProcess_WEBS.Add(sqldbid);
                }
                if(nobudgettype == false)
                {
                    SQLDBIDtoProcess_WEBS_To_Process.Add(sqldbid);
                }
                if (incompletesync == false)
                {
                    SQLDBIDtoProcess.Add(sqldbid);
                }
                //}
            }

            NotificationEmail.NotificationEmail.ProgressNotification("loop through list of sqldbids that will be included in this report");
            // loop through list of sqldbids that will be included in this report
            foreach (var sqldbid in SQLDBIDtoProcess)
            {

                lastsync = AllFranchises.AsEnumerable().Where(w => w.Field<byte>("sqldbid") == sqldbid && w.Field<DateTime>("lastsync") >= todate).Select(s => s.Field<DateTime>("lastsync")).FirstOrDefault();

                datafees = createdatafees(fdate, tdate, sqldbid);

                DataTable dfcalcppay = calctaxforprepayment(datafees);

                curCredCode = getcreditacctlist(sqldbid);

                bothreports = checkforoutofarea(sqldbid);

                processdata(datafees, sqldbid);

            }
            
            if(databaseName.IndexOf("LAWNDOCTORTRANSACTIONDB") > 0)
            {
                NotificationEmail.NotificationEmail.ProgressNotification("Do webs");
                foreach (var sqldbid in SQLDBIDtoProcess_WEBS_To_Process)
                {
                    doWEBS(sqldbid);
                }
            }
            
            NotificationEmail.NotificationEmail.ProgressNotification("Create service fees csv");
            createServicefeesCSV();

            NotificationEmail.NotificationEmail.ProgressNotification("Create cash comp csv");
            createCashCompCSV();

            if (databaseName.IndexOf("LAWNDOCTORTRANSACTIONDB") > 0)
            {
                NotificationEmail.NotificationEmail.ProgressNotification("create webscsv");
                createWEBSCSV();

                //This will create blank records for missing locations and budget types
                //We do not want the 0's going out to LD, hence it runs after the webs, we need it before the summary though
                FillMissingCompaniesWebs();

                NotificationEmail.NotificationEmail.ProgressNotification("Create webs summary csv");
                CreateWebsSummaryCSV();
            }
            NotificationEmail.NotificationEmail.ProgressNotification("Lawn Doctor Weekly Report, Process complete");
        }

        public static void doServiceFeeANDcashcompliance(Int32 comp_id, byte sqldbid)
        {
            updatefees(comp_id, sqldbid);

            doCashComplianceOnly(comp_id, sqldbid);
        }

        public static void doWEBS(byte sqldbid)
        {
            LawnDoctorTransactionDBEntities db = new LawnDoctorTransactionDBEntities();
            var adapter = (IObjectContextAdapter)db;
            var dbobjectContext = adapter.ObjectContext;

            var budgetlist = db.budgets.Where(w => w.SQLDBID == sqldbid).Select(s => s.budgetdesc).ToList();

            string sqlcmd;
            int season = todate.Year;

            // Create Table for WEBS report file

            if (isweekone == true)
            {
                sqlcmd = "exec LD_WEBS_Week_1_ProcessData " + sqldbid + ",'" + fdate + "','" + tdate + "'," + season;
            }
            else
            {
                sqlcmd = "exec LD_WEBS_ProcessData " + sqldbid + ",'" + fdate + "','" + tdate + "'," + season;
            }


            DataTable finalwebs = CommonTools.CommonTools.SQL_GetDT(sqlcmd, LawnDoctorReports.Properties.Settings.Default.LawnDoctorTransactionDBConnection);

            foreach (DataRow item in finalwebs.Rows)
            {
                var onecomp = AllFranchises.AsEnumerable().Where(w => w.Field<byte>("sqldbid") == sqldbid && w.Field<string>("AcctNo") == item.Field<string>("FranchiseAcct").Trim()).FirstOrDefault();

                if (onecomp != null)
                {
                    Dash_LD_WEBS webs = new Dash_LD_WEBS();

                    webs.sqldbid = Convert.ToInt16(sqldbid);
                    webs.comp_id = item.Field<Int32>("comp_id");
                    webs.AcctNo = item.Field<string>("FranchiseAcct").Trim();
                    webs.weeknumber = Convert.ToInt16(rpt_weeknum);
                    webs.weekenddate = todate;
                    webs.budgettype = item.Field<string>("type").Trim();
                    webs.leads = item.Field<Int32>("lead");
                    webs.changeinCIF = item.Field<Int32>("ccif");
                    webs.changeinBIF = item.Field<decimal>("cbif");
                    webs.CIFcancel = item.Field<Int32>("cifc");
                    webs.BIFcancel = item.Field<decimal>("bifc");
                    webs.CCsalesnumber = item.Field<Int32>("sn");
                    webs.CCsalesdollar = item.Field<decimal>("sd");
                    webs.CCcancelnumber = item.Field<Int32>("cn");
                    webs.CCcanceldollars = item.Field<decimal>("cd");
                    webs.CCnonrensalesnumber = item.Field<Int32>("nrsn");
                    webs.CCnonrensalesdollars = item.Field<decimal>("nrsd");
                    webs.CCnonrencancelnumber = item.Field<Int32>("nrcn");
                    webs.CCnonrencanceldollars = item.Field<decimal>("nrcd");
                    webs.CCrensalesnumber = item.Field<Int32>("rsn");
                    webs.CCrensalesdollars = item.Field<decimal>("rsd");
                    webs.CCrencancelnumber = item.Field<Int32>("rcn");
                    webs.CCrencanceldollars = item.Field<decimal>("rcd");


                    db.Dash_LD_WEBS.Add(webs);
                    try
                    {
                        db.SaveChanges();
                    }


                    catch (DbEntityValidationException dbEx)
                    {
                        foreach (var validationErrors in dbEx.EntityValidationErrors)
                        {
                            foreach (var validationError in validationErrors.ValidationErrors)
                            {
                                Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                                break;
                            }
                        }
                    }
                }
 
            }

            // add in missing budget types for each branch - only for those sqldbid's that have no missing budget types
            var branches = AllFranchises.AsEnumerable().Where(w => w.Field<byte>("sqldbid") == sqldbid).ToList();
            var masterbudget = db.Dash_LD_Master_BudgetType.Select(s => s.MasterBudgetType).ToList();

            foreach (var item in branches)
            {
                bool addSqldbid = SQLDBIDtoProcess_WEBS.Contains(item.Field<byte>("sqldbid"));

                if (addSqldbid == true)
                {
                    int company_no = item.Field<Int32>("comp_id");
                    foreach (string desc in masterbudget)
                    {
                        var missingbranch = db.Dash_LD_WEBS.Where(w => w.weeknumber == rpt_weeknum && w.sqldbid == sqldbid &&
                            w.comp_id == company_no && w.budgettype.Trim() == desc.Trim()).FirstOrDefault();
                        if (missingbranch == null)
                        {
                            Dash_LD_WEBS webs = new Dash_LD_WEBS();

                            webs.sqldbid = Convert.ToInt16(sqldbid);
                            webs.comp_id = item.Field<Int32>("comp_id");
                            webs.AcctNo = item.Field<string>("AcctNo").Trim();
                            webs.weeknumber = Convert.ToInt16(rpt_weeknum);
                            webs.weekenddate = todate;
                            webs.budgettype = desc.Trim();
                            webs.leads = 0;
                            webs.changeinCIF = 0;
                            webs.changeinBIF = 0;
                            webs.CIFcancel = 0;
                            webs.BIFcancel = 0;
                            webs.CCsalesnumber = 0;
                            webs.CCsalesdollar = 0;
                            webs.CCcancelnumber = 0;
                            webs.CCcanceldollars = 0;
                            webs.CCnonrensalesnumber = 0;
                            webs.CCnonrensalesdollars = 0;
                            webs.CCnonrencancelnumber = 0;
                            webs.CCnonrencanceldollars = 0;
                            webs.CCrensalesnumber = 0;
                            webs.CCrensalesdollars = 0;
                            webs.CCrencancelnumber = 0;
                            webs.CCrencanceldollars = 0;


                            db.Dash_LD_WEBS.Add(webs);
                            try
                            {
                                db.SaveChanges();
                            }
                            catch (DbEntityValidationException dbEx)
                            {
                                foreach (var validationErrors in dbEx.EntityValidationErrors)
                                {
                                    foreach (var validationError in validationErrors.ValidationErrors)
                                    {
                                        Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }


        }

        public static void doCashComplianceOnly(Int32 comp_id, int sqldbid)
        {
            LawnDoctorTransactionDBEntities db = new LawnDoctorTransactionDBEntities();
            var adapter = (IObjectContextAdapter)db;
            var dbobjectContext = adapter.ObjectContext;

            var outofarea = db.franchisefees.Where(w => w.sqldbid == sqldbid && w.comp_id == comp_id).Select(s => s.chargeoutofarea).FirstOrDefault();

            decimal actamounts = 0;
            decimal acttaxamounts = 0;
            decimal grandtottaxes = 0;

            // Paid amount
            var tmpcurPaid = datafees.AsEnumerable().Where(w => w["comp_id"].ToInt() == comp_id && w["transcode"].ToString().Trim() == "P"
                && w["revcode"].ToString().Trim() == "PAY").ToList();
            decimal myamt = 0;

            foreach (var item in tmpcurPaid)
            {
                myamt = item.Field<decimal>("actamount");
                actamounts = actamounts + Math.Abs(myamt);
            }

            // Tax amount
            var tmpcurTaxes = datafees.AsEnumerable().Where(w => w["comp_id"].ToInt() == comp_id).Select(s => s.Field<decimal>("taxamt"));

            foreach (var item in tmpcurTaxes)
            {
                acttaxamounts = acttaxamounts + Math.Round(Math.Abs(item),2,MidpointRounding.AwayFromZero);
            }

            // get total tax for report summary line .. not sum of denormalized cursor. oops

            grandtottaxes = datafees.AsEnumerable().Select(s => s.Field<decimal>("taxamt")).Sum();

            decimal mypayments = actamounts;
            decimal mytaxes = Math.Round(acttaxamounts,2,MidpointRounding.AwayFromZero);

            // we excluded getting taxes from servhist for installment or contract services.  currently the taxes are zero but we are going to be defensive
            // and state we want to ignore those records just in case we change the way we handle taxes on installment services in the future.

            decimal mycredits = getcredits(comp_id, "");
            decimal mycash = Math.Round(mypayments - mycredits - mytaxes, 2,MidpointRounding.AwayFromZero);

            // start looping through the data for this time period
            // new senario .. we've got got production for the period but no payments or adjustments .. we need to create
            // a record for that.

            var checkforpay_adj = datafees.AsEnumerable().Where(w => w["comp_id"].ToInt() == comp_id
                    && (w["transcode"].ToString().Trim() == "P" | w["transcode"].ToString().Trim() == "A")).ToList();

            if (checkforpay_adj == null)
            {
                Dash_LD_CashCompliance cashcomp = new Dash_LD_CashCompliance();

                cashcomp.sqldbid = sqldbid;
                cashcomp.comp_id = comp_id;
                cashcomp.weeknumber = rpt_weeknum;
                cashcomp.weekenddate = todate;
                cashcomp.cust_no = 0;
                cashcomp.last_name = "Nothing To Rpt";
                cashcomp.first_name = "for Period";
                cashcomp.check_no = "";
                cashcomp.amount = 0;
                cashcomp.in_area = 0;
                cashcomp.out_of_area = 0;
                cashcomp.total_tax = 0;
                cashcomp.cash_minus_tax = 0;

                db.Dash_LD_CashCompliance.Add(cashcomp);
                db.SaveChanges();

            }
            else
            {
                decimal myinval = 0;
                decimal myoutval = 0;
                decimal actamt = 0;
                string mycheckno = "";
                string p_or_a = "";

                foreach (var item in checkforpay_adj)
                {
                    p_or_a = item.Field<string>("transcode").Trim();

                    if (item.Field<string>("revcode").Trim() == "PAY")
                    {
                        myinval = 0;
                        myoutval = 0;
                        actamt = 0;
                        if (p_or_a == "P")
                        {
                            mycheckno = item.Field<string>("check_no") ?? "".Trim();

                            if (outofarea ?? false == true)
                            {
                                string zippy;
                                if (item.Field<string>("zipcode").Trim().Length > 5)
                                {
                                    zippy = item.Field<string>("zipcode").Trim().Substring(0, 5);
                                }
                                else
                                {
                                    zippy = item.Field<string>("zipcode").Trim();
                                }
                                byte id = item.Field<byte>("sqldbid");
                                var cntr = db.zipcodes.Where(w => w.SQLDBID == id && w.zip.Trim() == zippy).FirstOrDefault();

                                actamt = Math.Abs(item.Field<decimal>("actamount"));

                                if (cntr != null) // in area
                                {
                                    myinval = Math.Abs(item.Field<decimal>("actamount"));
                                    myoutval = 0;
                                }
                                else // out of area
                                {
                                    myinval = 0;
                                    myoutval = Math.Abs(item.Field<decimal>("actamount"));
                                }
                            }

                            else // does not charge out of area
                            {
                                actamt = Math.Abs(item.Field<decimal>("actamount"));
                                myinval = Math.Abs(item.Field<decimal>("actamount"));
                                myoutval = 0;
                            }
                            Dash_LD_CashCompliance cashcomp = new Dash_LD_CashCompliance();

                            cashcomp.sqldbid = sqldbid;
                            cashcomp.comp_id = comp_id;
                            cashcomp.weeknumber = rpt_weeknum;
                            cashcomp.weekenddate = todate;
                            cashcomp.cust_no = item.Field<int>("cust_no");
                            cashcomp.last_name = item.Field<string>("lastname");
                            cashcomp.first_name = item.Field<string>("firstname");
                            cashcomp.check_no = mycheckno;
                            cashcomp.amount = actamt;
                            cashcomp.in_area = myinval;
                            cashcomp.out_of_area = myoutval;
                            cashcomp.total_tax = mytaxes;
                            cashcomp.cash_minus_tax = mycash;
                            cashcomp.histdate = item.Field<DateTime>("date");

                            db.Dash_LD_CashCompliance.Add(cashcomp);
                            db.SaveChanges();
                        }

                        if (p_or_a == "A")
                        {
                            mycheckno = item.Field<string>("adjcode") ?? "".Trim();

                            // ok this is an adjustment .. we need to see if it is a credit against ACCOUNTS RECEIVABLE GL account
                            var adjcodeval = curCredCode.Where(w => w.adjcode.Trim() == mycheckno.Trim()).FirstOrDefault();
                            if (adjcodeval != null)
                            {
                                if (outofarea ?? false == true)
                                {
                                    string zippy;
                                    if (item.Field<string>("zipcode").Trim().Length > 5)
                                    {
                                        zippy = item.Field<string>("zipcode").Trim().Substring(0, 5);
                                    }
                                    else
                                    {
                                        zippy = item.Field<string>("zipcode").Trim();
                                    }
                                    byte id = item.Field<byte>("sqldbid");
                                    var cntr = db.zipcodes.Where(w => w.SQLDBID == id && w.zip.Trim() == zippy).FirstOrDefault();

                                    actamt = -1 * item.Field<decimal>("actamount");

                                    if (cntr != null) // in area
                                    {
                                        myinval = -1 * item.Field<decimal>("actamount");
                                        myoutval = 0;
                                    }
                                    else // out of area
                                    {
                                        myinval = 0;
                                        myoutval = myinval = -1 * item.Field<decimal>("actamount");
                                    }
                                }

                                else // does not charge out of area
                                {
                                    actamt = -1 * item.Field<decimal>("actamount");
                                    myinval = myinval = -1 * item.Field<decimal>("actamount");
                                    myoutval = 0;
                                }

                                Dash_LD_CashCompliance cashcomp = new Dash_LD_CashCompliance();

                                cashcomp.sqldbid = sqldbid;
                                cashcomp.comp_id = comp_id;
                                cashcomp.weeknumber = rpt_weeknum;
                                cashcomp.weekenddate = todate;
                                cashcomp.cust_no = item.Field<int>("cust_no");
                                cashcomp.last_name = item.Field<string>("lastname");
                                cashcomp.first_name = item.Field<string>("firstname");
                                cashcomp.check_no = mycheckno;
                                cashcomp.amount = actamt;
                                cashcomp.in_area = myinval;
                                cashcomp.out_of_area = myoutval;
                                cashcomp.total_tax = mytaxes;
                                cashcomp.cash_minus_tax = mycash;
                                cashcomp.histdate = item.Field<DateTime>("date");

                                db.Dash_LD_CashCompliance.Add(cashcomp);
                                db.SaveChanges();
                            }

                        }

                    }


                }
            }

            var checkrecs = db.Dash_LD_CashCompliance.Where(w => w.comp_id == comp_id && w.sqldbid == sqldbid && w.weekenddate == todate).FirstOrDefault();
            if (checkrecs == null)
            {
                Dash_LD_CashCompliance cashcomp = new Dash_LD_CashCompliance();
                
                cashcomp.sqldbid = sqldbid;
                cashcomp.comp_id = comp_id;
                cashcomp.weeknumber = rpt_weeknum;
                cashcomp.weekenddate = todate;
                cashcomp.cust_no = 0;
                cashcomp.last_name = "Nothing To Rpt";
                cashcomp.first_name = "for Period";
                cashcomp.check_no = "";
                cashcomp.amount = 0;
                cashcomp.in_area = 0;
                cashcomp.out_of_area = 0;
                cashcomp.total_tax = 0;
                cashcomp.cash_minus_tax = 0;
                
                db.Dash_LD_CashCompliance.Add(cashcomp);
                db.SaveChanges();
            }


        }
        public static void FillMissingCompaniesWebs()
        {
            var sqlQuery = @"declare @weekenddate date = '" + todate.ToString("yyyy-MM-dd") + "', @weeknumber int = " + rpt_weeknum.ToString().Trim() + @"
insert into Dash_LD_WEBS (budgettype, comp_id, sqldbid, AcctNo, weekenddate, weeknumber,
leads,changeinCIF,changeinBIF,CIFcancel,BIFcancel,CCsalesnumber,CCsalesdollar,CCcancelnumber,CCcanceldollars,CCnonrensalesnumber,CCnonrensalesdollars,CCnonrencancelnumber,
CCnonrencanceldollars,CCrensalesnumber,CCrensalesdollars,CCrencancelnumber,CCrencanceldollars)
select distinct a.MasterBudgetType, a.comp_id, a.SQLDBID, a.branchno, @weekenddate, @weeknumber,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
from (select comp.MasterBudgetType, comp.comp_id, comp.SQLDBID, comp.branchno from (select b.MasterBudgetType, c.* From company c, (select distinct MasterBudgetType from Dash_LD_Master_BudgetType) b where isnull(branchno,'') <> '') comp
left join (select distinct d.sqldbid, d.comp_id, d.budgettype from Dash_LD_WEBS d inner join (select distinct MasterBudgetType from Dash_LD_Master_BudgetType) m on d.budgettype = m.MasterBudgetType where weekenddate = @weekenddate) x on comp.SQLDBID=x.sqldbid and comp.comp_Id = x.comp_Id and comp.MasterBudgetType = x.budgettype 
where isnull(branchno,'') <> '' and x.sqldbid is null and x.comp_id is null) a
order by a.sqldbid, a.comp_id
";
            CommonTools.CommonTools.SQL_GetDT(sqlQuery, LawnDoctorReports.Properties.Settings.Default.LawnDoctorTransactionDBConnection);
        }
        public static void CreateConditionCodeSummaryCSV()
        {
            if(string.IsNullOrWhiteSpace(mailSubjectPrefix))
            {
                LawnDoctorTransactionDBEntities db = new LawnDoctorTransactionDBEntities();
                var adapter = (IObjectContextAdapter)db;
                var dbobjectContext = adapter.ObjectContext;
                string sqlQuery = "declare @start date = '" + fdate + "'";
                sqlQuery += Environment.NewLine + "declare @end date = '" + tdate + "'";
                sqlQuery += Environment.NewLine + @"select market.market,  
isnull(a.[Conditions Used this Month],0) as conditionsUsed,  
isnull(b.[Conditions That Sell],0) sellConditionsUsed,  
isnull(d.[conditions used ytd],0) ConditionsYTD, 
isnull(e.[conditions sell ytd],0) CondtionsSellYtd, 
c.activeCustomers  
from market  
left join(  
select market.market, count(*)'Conditions Used this Month' from servcond  
inner join condcode on servcond.condcode=condcode.condcode and condcode.SQLDBID=servcond.SQLDBID  
inner join servhist on servcond.service_id=servhist.service_id and servhist.SQLDBID=servcond.SQLDBID  
inner join company on company.comp_id = servhist.comp_id and company.SQLDBID=servhist.SQLDBID  
inner join market on company.marketid = market.marketid and market.SQLDBID=company.SQLDBID  
where market.sqldbid = 143 and convert(date, servhist.donedate) between @start and @end  
group by market.market) a on market.market=a.market and market.SQLDBID=143  
left join(  
select market.market, count(*)'Conditions That Sell' from servcond  
inner join condcode on servcond.condcode=condcode.condcode and condcode.SQLDBID=servcond.SQLDBID  
inner join servhist on servcond.service_id=servhist.service_id and servhist.SQLDBID=servcond.SQLDBID  
inner join company on company.comp_id = servhist.comp_id and company.SQLDBID=servhist.SQLDBID  
inner join market on company.marketid = market.marketid and market.SQLDBID=company.SQLDBID  
where convert(date, servhist.donedate) between @start and @end  
and servcond.sqldbid = 143 and condcode.progdefid is not null  
group by market.market) b on market.market=b.market and market.SQLDBID=143  
left join(  
select market.market, count(*)'Conditions Used YTD' from servcond  
inner join condcode on servcond.condcode=condcode.condcode and condcode.SQLDBID=servcond.SQLDBID  
inner join servhist on servcond.service_id=servhist.service_id and servhist.SQLDBID=servcond.SQLDBID  
inner join company on company.comp_id = servhist.comp_id and company.SQLDBID=servhist.SQLDBID  
inner join market on company.marketid = market.marketid and market.SQLDBID=company.SQLDBID  
where market.sqldbid = 143 and year(convert(date, servhist.donedate)) = year(@start)  
group by market.market) d on market.market=d.market and market.SQLDBID=143  
left join(  
select market.market, count(*)'Conditions Sell YTD' from servcond  
inner join condcode on servcond.condcode=condcode.condcode and condcode.SQLDBID=servcond.SQLDBID  
inner join servhist on servcond.service_id=servhist.service_id and servhist.SQLDBID=servcond.SQLDBID  
inner join company on company.comp_id = servhist.comp_id and company.SQLDBID=servhist.SQLDBID  
inner join market on company.marketid = market.marketid and market.SQLDBID=company.SQLDBID  
where market.sqldbid = 143 and year(convert(date, servhist.donedate))=year(@start)  
and condcode.progdefid is not null 
group by market.market) e on market.market=e.market and market.SQLDBID=143  
inner join (  
select market,count(*) activeCustomers From market  
inner join company on market.SQLDBID=company.SQLDBID and market.marketid=company.marketid  
inner join customer on company.comp_id = customer.comp_Id and company.SQLDBID=customer.SQLDBID  
where customer.status>7 and market.SQLDBID=143  
group by market) c on market.market = c.market and market.SQLDBID=143  
where market.SQLDBID=143  
group by market.market,[Conditions Used this Month],[Conditions That Sell],c.activeCustomers,d.[Conditions Used YTD],e.[Conditions Sell YTD] 
order by market
";
                DataTable conditionSummary = CommonTools.CommonTools.SQL_GetDT(sqlQuery, LawnDoctorReports.Properties.Settings.Default.LawnDoctorTransactionDBConnection);

                List<ConditionSummaryObject> results = new List<ConditionSummaryObject>();

                foreach (DataRow item in conditionSummary.Rows)
                {
                    ConditionSummaryObject summary = new ConditionSummaryObject();
                    summary.market = item.Field<string>("market").Trim();
                    summary.conditionsUsed = item.Field<Int32>("conditionsUsed");
                    summary.sellConditionsUsed = item.Field<Int32>("sellConditionsUsed");
                    summary.conditionsUsedYTD = item.Field<Int32>("ConditionsYTD");
                    summary.sellConditionsUsedYTD = item.Field<Int32>("CondtionsSellYtd");
                    summary.activeCustomers = item.Field<Int32>("activeCustomers");
                    results.Add(summary);
                }
                string dateOnly = Convert.ToDateTime(todate).Date.ToString("MM_dd_yyyy");

                string mypath = mypathstart + "\\" + dateOnly.Trim();
                Directory.CreateDirectory(@mypath);

                //string subfolders;

                //subfolders = mypath + "\\CASS";
                //Directory.CreateDirectory(@subfolders);
                //subfolders = mypath + "\\CASS\\batch";
                //Directory.CreateDirectory(@subfolders);
                //subfolders = mypath + "\\CASS\\seed";
                //Directory.CreateDirectory(@subfolders);
                //subfolders = mypath + "\\Final";
                //Directory.CreateDirectory(@subfolders);
                //subfolders = mypath + "\\Proof";
                //Directory.CreateDirectory(@subfolders);

                string mypathdata = mypath + "\\" + dateOnly.Trim() + "_" + rpt_weeknum.ToString().Trim() + "ConditionCodeSummary.csv";
                string csv = LawnDoctorReports.Common.ToCsv<ConditionSummaryObject>(",", results);
                TextWriter tw = new StreamWriter(mypathdata, false);
                tw.Write(csv);
                tw.Close();

                string toemailaddy = "mcarden@realgreen.com";
                string mailSubject = mailSubjectPrefix + "Condition code summary Week : " + rpt_weeknum.ToString().Trim();
                NotificationEmail.NotificationEmail.EmailReport(mypathdata, mailSubject, toemailaddy);
            }
        }
        public static void createServicefeesCSV()
        {

                LawnDoctorTransactionDBEntities db = new LawnDoctorTransactionDBEntities();

                List<servicefeeCorpcls> sfFile = new List<servicefeeCorpcls>();

                var servicefeeoneweek = db.Dash_LD_servicefeeCorp.Where(w => w.weekenddate == todate);

                foreach (var item in servicefeeoneweek)
                {
                    servicefeeCorpcls singleacct = new servicefeeCorpcls
                    {
                        franchise_acct = item.franchise_acct.PadLeft(4,'0'),
                        franchise_name = item.franchise_name,
                        weeknumber = item.weeknumber,
                        weekenddate = item.weekenddate,
                        cash_minus_tax = item.cash_minus_tax ,
                        type = item.type,
                        amount_due = item.amount_due ,
                        percentage = item.percentage,
                        doc_num = item.doc_num,
                        last_sync = item.last_sync,
                        cash_minus_tax_in = item.cash_minus_tax_in,
                        cash_minus_tax_out = item.cash_minus_tax_out
                    };

                    sfFile.Add(singleacct);
                }

                string dateOnly = Convert.ToDateTime(todate).Date.ToString("MM_dd_yyyy");

                string mypath = mypathstart + "\\" + dateOnly.Trim() ;
                Directory.CreateDirectory(@mypath);

                //string subfolders;

                //subfolders = mypath + "\\CASS";
                //Directory.CreateDirectory(@subfolders);
                //subfolders = mypath + "\\CASS\\batch";
                //Directory.CreateDirectory(@subfolders);
                //subfolders = mypath + "\\CASS\\seed";
                //Directory.CreateDirectory(@subfolders);
                //subfolders = mypath + "\\Final";
                //Directory.CreateDirectory(@subfolders);
                //subfolders = mypath + "\\Proof";
                //Directory.CreateDirectory(@subfolders);

                string mypathdata = mypath + "\\" + dateOnly.Trim() + "_" + rpt_weeknum.ToString().Trim() + "_ServiceFee.csv";
                string csv = LawnDoctorReports.Common.ToCsv<servicefeeCorpcls>(",", sfFile);
                TextWriter tw = new StreamWriter(mypathdata, false);
                tw.Write(csv);
                tw.Close();

                string toemailaddy = "lawndoctorreports@realgreen.com";
                string mailSubject = mailSubjectPrefix + "Service Fee Report for Week: " + rpt_weeknum.ToString().Trim();
                NotificationEmail.NotificationEmail.EmailReport(mypathdata,mailSubject,toemailaddy);

    }

        public static void createCashCompCSV()
        {

            LawnDoctorTransactionDBEntities db = new LawnDoctorTransactionDBEntities();

            List<CashCompcls> ccFile = new List<CashCompcls>();

            var cashcomponeweek = db.Dash_LD_CashCompliance.Where(w => w.weekenddate == todate);

            foreach (var item in cashcomponeweek)
            {
  
                var onecomp = AllFranchises.AsEnumerable().Where(w => w.Field<byte>("sqldbid") == item.sqldbid && w.Field<Int32>("comp_id") == item.comp_id).FirstOrDefault();

                string acct = onecomp.Field<string>("acctno");
                acct = acct.Trim();

                string franchisename = onecomp.Field<string>("lawn doctor of");
                franchisename = franchisename.Trim();

                CashCompcls singleacct = new CashCompcls
                {
                    
                    franchise_acct = Franchise_Account_Prefix + acct.PadLeft(4, '0'),
                    franchise_name = franchisename,
                    weeknumber = item.weeknumber,
                    weekenddate = item.weekenddate,
                    cust_no = item.cust_no,
                    last_name = item.last_name,
                    first_name = item.first_name,
                    histdate = item.histdate ?? todate,
                    check_no = item.check_no,
                    amount = item.amount,
                    in_area = item.in_area ?? 0,
                    out_of_area = item.out_of_area ?? 0,
                    total_tax = item.total_tax ?? 0,
                    cash_minus_tax = item.cash_minus_tax ?? 0,
                };

                ccFile.Add(singleacct);
            }

            string dateOnly = Convert.ToDateTime(todate).Date.ToString("MM_dd_yyyy");

            string mypath = mypathstart + "\\" + dateOnly.Trim();
            Directory.CreateDirectory(@mypath);

            //string subfolders;

            //subfolders = mypath + "\\CASS";
            //Directory.CreateDirectory(@subfolders);
            //subfolders = mypath + "\\CASS\\batch";
            //Directory.CreateDirectory(@subfolders);
            //subfolders = mypath + "\\CASS\\seed";
            //Directory.CreateDirectory(@subfolders);
            //subfolders = mypath + "\\Final";
            //Directory.CreateDirectory(@subfolders);
            //subfolders = mypath + "\\Proof";
            //Directory.CreateDirectory(@subfolders);

            string mypathdata = mypath + "\\" + dateOnly.Trim() + "_" + rpt_weeknum.ToString().Trim() + "_CashCompliance.csv";
            string csv = LawnDoctorReports.Common.ToCsv<CashCompcls>(",", ccFile);
            TextWriter tw = new StreamWriter(mypathdata, false);
            tw.Write(csv);
            tw.Close();

            string toemailaddy = "lawndoctorreports@realgreen.com";
            string mailSubject = mailSubjectPrefix + "Cash Compliance Report for Week: " + rpt_weeknum.ToString().Trim();
            NotificationEmail.NotificationEmail.EmailReport(mypathdata, mailSubject, toemailaddy);

        }
        public static DataTable createdatafees(string fromdate, string todate, int sqldbid)
        {

            string sqlcmd;

            LawnDoctorTransactionDBEntities db = new LawnDoctorTransactionDBEntities();

            // gather payments and adustments

            sqlcmd = "exec LD_ServiceFee_ProcessData " + sqldbid + ",'" + fdate + "','" + tdate + "'";

            DataTable datafees = CommonTools.CommonTools.SQL_GetDT(sqlcmd, LawnDoctorReports.Properties.Settings.Default.LawnDoctorTransactionDBConnection);

            return datafees;

        }


        public static void createWEBSCSV()
        {

            LawnDoctorTransactionDBEntities db = new LawnDoctorTransactionDBEntities();

            List<WEBScls> ccFile = new List<WEBScls>();

            var WEBSoneweek = db.Dash_LD_WEBS.Where(w => w.weekenddate == todate).OrderBy(o => o.AcctNo).ThenBy(o => o.budgettype);

            foreach (var item in WEBSoneweek)
            {

                var onecomp = AllFranchises.AsEnumerable().Where(w => w.Field<byte>("sqldbid") == item.sqldbid && w.Field<string>("AcctNo") == item.AcctNo).FirstOrDefault();
                
                string acct = onecomp.Field<string>("acctno");
                acct = acct.Trim();

                string franchisename = onecomp.Field<string>("lawn doctor of");
                franchisename = franchisename.Trim();

                WEBScls singleacct = new WEBScls
                {
                    franchise_acct = Franchise_Account_Prefix + acct.PadLeft(4, '0'),
                    franchise_name = franchisename,
                    weeknumber = item.weeknumber,
                    weekenddate = item.weekenddate,
                    type = item.budgettype,
                    lead = item.leads ?? 0,
                    sales_no = item.CCsalesnumber ?? 0,
                    sales_dollars = item.CCsalesdollar ?? 0,
                    cancel_no = item.CCcancelnumber ?? 0,
                    cancel_dollars = item.CCcanceldollars ?? 0,
                    non_renew_sales_no = item.CCnonrensalesnumber ?? 0,
                    non_renew_sales_dollars = item.CCnonrensalesdollars ?? 0,
                    renew_sales_no = item.CCrensalesnumber ?? 0,
                    renew_sales_dollars = item.CCrensalesdollars ?? 0,
                    non_renew_cancel_no = item.CCnonrencancelnumber ?? 0,
                    non_renew_cancel_dollars = item.CCnonrencanceldollars ?? 0,
                    renew_cancel_no = item.CCrencancelnumber ?? 0,
                    renew_cancel_dollars = item.CCrencanceldollars ?? 0,
                    change_in_CIF = item.changeinCIF ?? 0,
                    change_in_BIF = item.changeinBIF ?? 0,
                    CIF_cancel = item.CIFcancel ?? 0,
                    BIF_cancel = item.BIFcancel ?? 0
                };

                ccFile.Add(singleacct);
            }

            string dateOnly = Convert.ToDateTime(todate).Date.ToString("MM_dd_yyyy");

            string mypath = mypathstart + "\\" + dateOnly.Trim();
            Directory.CreateDirectory(@mypath);

            //string subfolders;

            //subfolders = mypath + "\\CASS";
            //Directory.CreateDirectory(@subfolders);
            //subfolders = mypath + "\\CASS\\batch";
            //Directory.CreateDirectory(@subfolders);
            //subfolders = mypath + "\\CASS\\seed";
            //Directory.CreateDirectory(@subfolders);
            //subfolders = mypath + "\\Final";
            //Directory.CreateDirectory(@subfolders);
            //subfolders = mypath + "\\Proof";
            //Directory.CreateDirectory(@subfolders);

            string mypathdata = mypath + "\\" + dateOnly.Trim() + "_" + rpt_weeknum.ToString().Trim() + "_WEBS.csv";
            string csv = LawnDoctorReports.Common.ToCsv<WEBScls>(",", ccFile);
            TextWriter tw = new StreamWriter(mypathdata, false);
            tw.Write(csv);
            tw.Close();

            string toemailaddy = "lawndoctorreports@realgreen.com";
            string mailSubject = mailSubjectPrefix + "WEBS Report for Week: " + rpt_weeknum.ToString().Trim();
            NotificationEmail.NotificationEmail.EmailReport(mypathdata, mailSubject, toemailaddy);

        }
        public static void CreateWebsSummaryCSV()
        {
            sqlcmd = "Exec LD_FranchiseTable";
            CommonTools.CommonTools.SQL_GetDT(sqlcmd, LawnDoctorReports.Properties.Settings.Default.LawnDoctorTransactionDBConnection);

            DataTable dt = CommonTools.CommonTools.SQL_GetDT("ld_webs_summary", LawnDoctorReports.Properties.Settings.Default.LawnDoctorTransactionDBConnection);
            List<LDWebsSummary> ldWebsSummaryList = new List<LDWebsSummary>();
            foreach (DataRow row in (InternalDataCollectionBase)dt.Rows)
                ldWebsSummaryList.Add(new LDWebsSummary()
                {
                    AcctNo = row["AcctNo"].ToString().Trim(),
                    Franchise = (string)row["Franchise"],
                    weeknumber = (int)row["weeknumber"],
                    weekenddate = row["weekenddate"].ToString().Trim(),
                    budgettype = (string)row["budgettype"],
                    CCsalesnumber = (int)row["CCsalesnumber"],
                    CCsalesdollar = (Decimal)(row["CCsalesdollar"] ?? (object)0),
                    CCcancelnumber = (int)row["CCcancelnumber"],
                    CCcanceldollars = (Decimal)row["CCcanceldollars"],
                    YTD_cc_sales_no = (int)row["YTD_cc_sales_no"],
                    YTD_cc_Sales_dollar = (Decimal)row["YTD_cc_Sales_dollar"],
                    YTD_cc_cancel_no = (int)row["YTD_cc_cancel_no"],
                    YTD_cc_cancel_dollar = (Decimal)row["YTD_cc_cancel_dollar"],
                    CC_Active_Count = (int)row["CC_Active_Count"],
                    cc_active_dollar = (Decimal)row["cc_active_dollar"],
                    NS_Cif = (int)row["NS_Cif"],
                    NS_BIF = (Decimal)row["NS_BIF"],
                    NS_sqft = (Decimal)row["NS_sqft"],
                    CIFCancel = (int)row["CIFCancel"],
                    BIFCancel = (Decimal)row["BIFCancel"],
                    YTD_NS_CIF = (int)row["YTD_NS_CIF"],
                    YTD_NS_BIF = (Decimal)row["YTD_NS_BIF"],
                    YTD_NS_sqft = (Decimal)row["YTD_NS_sqft"],
                    YTD_CIF_Cancel = (int)row["YTD_CIF_Cancel"],
                    YTD_BIF_Cancel = (Decimal)row["YTD_BIF_Cancel"],
                    Active_Program_CIF = (int)row["Active_Program_CIF"],
                    Active_Program_BIF = (Decimal)row["Active_Program_BIF"],
                    active_program_sqft = (Decimal)row["active_program_sqft"]
                });
            string str1 = Convert.ToDateTime(Program.todate).Date.ToString("MM_dd_yyyy");
            string path = Program.mypathstart + "\\" + str1.Trim();
            Directory.CreateDirectory(path);
            string str2 = path + "\\" + str1.Trim() + "_" + Program.rpt_weeknum.ToString().Trim() + "_WebsSummary.csv";
            string csv = Common.ToCsv<LDWebsSummary>(",", (IEnumerable<LDWebsSummary>)ldWebsSummaryList);
            TextWriter textWriter = (TextWriter)new StreamWriter(str2, false);
            textWriter.Write(csv);
            textWriter.Close();
            string to = "lawndoctorreports@realgreen.com";
            string subject = mailSubjectPrefix + "Webs Summary report for Week: " + Program.rpt_weeknum.ToString().Trim();
            NotificationEmail.NotificationEmail.EmailReport(str2, subject, to);

        }
        public static DataTable calctaxforprepayment(DataTable datafeecalc)
        {

            //*  METHOD DESCRIPTION	::
            //*       this method is going to take the datafees table and update the taxes paid for
            //*		the prepayments of INSTALLMENTS ONLY.  We are calculatig tax for prepaid services in process data
            //*       using histhist table to determine which services were marked as prepaid from posted payment that period
            //* 	knowns:  REVCODE is 'TAX'
            //*			 TRANSCODE is 'P' 
            //*			 PREPAYID IS -2
            //*	assumption:
            //*			if the customer has any tax rate the entire payment is considered taxable.
            //*	process:
            //*		We are going to take the actamount and use the customer number to determine their tax rate(s) and back the taxes out of the 
            //*		actamount .. need to update tax1,tax2,tax3 and taxamt for subsequent processing.

            //*		this method needs to be called prior to using the data fees table .. so we will call it right after
            //*		creating the table.


            LawnDoctorTransactionDBEntities db = new LawnDoctorTransactionDBEntities();

            foreach (DataRow item in datafeecalc.Rows)
            {
                //double? taxrate1 = 0.00000;
                //double? taxrate2 = 0.00000;
                //double? taxrate3 = 0.00000;
                //double? efftax = 0.00000;
                //double? efftax1 = 0.00000;
                //double? efftax2 = 0.00000;
                //double? efftax3 = 0.00000;
                //double? efftaxtotal = 0.00000;
                //double? amtb4tax = 0.00000;
                //double? taxamt4update = 0.00000;
                //double? tax14update = 0.00000;
                //double? tax24update = 0.00000;
                //double? tax34update = 0.00000;

                decimal? taxrate1 = 0;
                decimal? taxrate2 = 0;
                decimal? taxrate3 = 0;
                decimal? efftax = 0;
                decimal? efftax1 = 0;
                decimal? efftax2 = 0;
                decimal? efftax3 = 0;
                decimal? efftaxtotal = 0;
                decimal? amtb4tax = 0;
                decimal? amtb4taxZ = 0;
                decimal? taxamt4update = 0;
                decimal? tax14update = 0;
                decimal? tax24update = 0;
                decimal? tax34update = 0;

                int cust_no = item["cust_no"].ToInt();
                byte sqlid = (item.Field<byte>("sqldbid"));
                customer paycustone = (customer)db.customers.Where(w => w.cust_no == cust_no && w.SQLDBID == sqlid).FirstOrDefault();

                if (item["revcode"].ToString().Trim().ToUpper() == "TAX" && item["transcode"].ToString().Trim().ToUpper() == "P"
                     && item["prepayid"].ToInt() == -2)
                {

                    if (paycustone.taxid1 != null)
                    {
                        taxid availtax = (taxid)db.taxids.Where(w => w.available == true && w.taxid1 == paycustone.taxid1 && w.SQLDBID == sqlid).FirstOrDefault();

                        if (availtax == null)
                        {
                            taxrate1 = 0;
                        }
                        else
                        {
                            taxrate1 = (availtax.taxrate);
                            // JJZ taxrate1 = Convert.ToDouble(availtax.taxrate);
                        }
                    }

                    // tax rate 2

                    if (paycustone.taxid2 != null)
                    {
                        taxid availtax = (taxid)db.taxids.Where(w => w.available == true && w.taxid1 == paycustone.taxid2 && w.SQLDBID == sqlid).FirstOrDefault();

                        if (availtax == null)
                        {
                            taxrate2 = 0;
                        }
                        else
                        {
                            taxrate2 = (availtax.taxrate);
                            // JJZ taxrate2 = Convert.ToDouble(availtax.taxrate);
                        }
                    }

                    // tax rate 3

                    if (paycustone.taxid3 != null)
                    {
                        taxid availtax = (taxid)db.taxids.Where(w => w.available == true && w.taxid1 == paycustone.taxid3 && w.SQLDBID == sqlid).FirstOrDefault();

                        if (availtax == null)
                        {
                            taxrate3 = 0;
                        }
                        else
                        {
                            taxrate3 = (availtax.taxrate);
                            // JJZ taxrate3 = Convert.ToDouble(availtax.taxrate);
                        }
                    }


                    efftax = taxrate1 + taxrate2 + taxrate3;
                    if (efftax >= 0)
                    {
                        efftaxtotal = (efftax / 100);
                        efftax1 = taxrate1 / 100;
                        efftax2 = taxrate2 / 100;
                        efftax3 = taxrate3 / 100;

                        // JJZ amtb4tax = Math.Abs(Convert.ToDouble(item["actamount"])) / (1 + efftaxtotal);
                        //amtb4tax = (double)Math.Round((decimal)amtb4tax, 2, MidpointRounding.AwayFromZero);
                        //double? amt4update = (amtb4tax * efftaxtotal);
                        //taxamt4update = (double)Math.Round((decimal)amt4update, 2, MidpointRounding.AwayFromZero);
                        //tax14update = (double)Math.Round((decimal)amtb4tax * (decimal)efftax1, 2, MidpointRounding.AwayFromZero);
                        //tax24update = (double)Math.Round((decimal)amtb4tax * (decimal)efftax2, 2, MidpointRounding.AwayFromZero);
                        //tax34update = (double)Math.Round((decimal)amtb4tax * (decimal)efftax3, 2, MidpointRounding.AwayFromZero);

                        amtb4taxZ = Math.Abs(Convert.ToDecimal(item["actamount"])) / (1 + efftaxtotal);
                        amtb4tax = (decimal)Math.Round((decimal)amtb4taxZ, 2, MidpointRounding.AwayFromZero);
                        decimal? amt4update = (amtb4tax * efftaxtotal);
                        taxamt4update = (decimal)Math.Round((decimal)amt4update, 2, MidpointRounding.AwayFromZero);
                        tax14update = (decimal)Math.Round((decimal)amtb4tax * (decimal)efftax1, 2, MidpointRounding.AwayFromZero);
                        tax24update = (decimal)Math.Round((decimal)amtb4tax * (decimal)efftax2, 2, MidpointRounding.AwayFromZero);
                        tax34update = (decimal)Math.Round((decimal)amtb4tax * (decimal)efftax3, 2, MidpointRounding.AwayFromZero);

                        item["tax1"] = tax14update;
                        item["tax2"] = tax24update;
                        item["tax3"] = tax34update;
                        item["taxamt"] = taxamt4update;
                    }
                }
            }

            DataTable dt = datafeecalc;
            return dt;
        }

        public static void processdata(DataTable df, byte sqldbid)
        {
            LawnDoctorTransactionDBEntities db = new LawnDoctorTransactionDBEntities();
            var mycomps = AllFranchises.AsEnumerable().Where(w => w.Field<byte>("sqldbid") == sqldbid).ToList();

            foreach (var id in mycomps)
            {
                doServiceFeeANDcashcompliance(id.Field<Int32>("comp_id") ,sqldbid);

           }
            //return
        }

        public static void updatefees(Int32 comp_id, byte sqldbid)
        {
            LawnDoctorTransactionDBEntities db = new LawnDoctorTransactionDBEntities();
            var adapter = (IObjectContextAdapter)db;
            var dbobjectContext = adapter.ObjectContext;

            decimal mycashin = getservicein(comp_id);
            decimal mycashout = getserviceout(comp_id);

            var onecomp = AllFranchises.AsEnumerable().Where(w => w.Field<byte>("sqldbid") == sqldbid && w.Field<Int32>("comp_id") == comp_id ).FirstOrDefault();

            string acct = onecomp.Field<string>("acctno");
            acct = acct.Trim().PadLeft(3, '0');

            string name = onecomp.Field<string>("lawn doctor of");
            name = name.Trim();

            var royaltyinfo = db.franchisefees.Where(w => w.sqldbid == sqldbid && w.comp_id == comp_id).FirstOrDefault();
            if (royaltyinfo == null)
                return;
            if (databaseName.IndexOf("LAWNDOCTORTRANSACTIONDB") < 0)
            {
                royalty_in = royaltyinfo.royalty ?? 10;
                royalty_out = royaltyinfo.outofarea ?? 25;
            }

            decimal advertisement = 0.00m;
            if (decimal.TryParse(onecomp.Field<string>("Reg Adv Percent").Trim(), out advertisement))
            {
                advertisement = Convert.ToDecimal(onecomp.Field<string>("Reg Adv Percent").Trim());
            }
            

                bool chargeout = royaltyinfo.chargeoutofarea ?? false;
            if (chargeout == false)
            {
                mycashin = mycashin + mycashout;
                mycashout = 0;
            }

            //decimal myadvfee = Math.Round((((mycashin + mycashout) * advertisement) / 100), 2);
            //decimal myservfee = Math.Round(((mycashin * royalty_in) / 100), 2);
            //decimal myooafee = Math.Round(((mycashout * royalty_out) / 100), 2);

            decimal myadvfee =  Math.Round(((mycashin + mycashout) * (advertisement) / 100), 2, MidpointRounding.AwayFromZero);
            decimal myservfee = Math.Round((mycashin  * (royalty_in   / 100)), 2, MidpointRounding.AwayFromZero);
            decimal myooafee =  Math.Round((mycashout * (royalty_out  / 100)), 2, MidpointRounding.AwayFromZero);

            string docyear;
            string docfranchise;
            string docweek;
            string doctype;

            // add to dash_LD_service_fee - used on dashboard

            Dash_LD_servicefee servicefee = new Dash_LD_servicefee();
            if (chargeout == true)
            {
                servicefee.showinout = true;
            }
            else
            {
                servicefee.showinout = false;
            }

            servicefee.sqldbid = sqldbid;
            servicefee.comp_id = comp_id;
            servicefee.franchise_acct = acct;
            servicefee.franchise_name = name; 
            servicefee.weekstartdate = fromdate;
            servicefee.weekenddate = todate;
            servicefee.weeknumber = rpt_weeknum;
            servicefee.percentage = advertisement;
            servicefee.advfee = myadvfee;
            servicefee.amount_due = myservfee + myooafee + myadvfee;
            servicefee.cash_minus_tax = mycashin + mycashout;
            servicefee.cash_minus_tax_in = mycashin;
            servicefee.cash_minus_tax_out = mycashout;
            servicefee.service_fee = myservfee + myooafee;
            servicefee.service_fee_in = myservfee;
            servicefee.service_fee_out = myooafee;
            servicefee.last_sync = lastsync;

            db.Dash_LD_servicefee.Add(servicefee);

            db.SaveChanges();

            // add to table used to create file for LD Corp automated reporting
            if (myadvfee != 0)
            {
                Dash_LD_servicefeeCorp servicefeeC1 = new Dash_LD_servicefeeCorp();

                servicefeeC1.franchise_acct = Franchise_Account_Prefix + acct.PadLeft(4, '0');
                servicefeeC1.franchise_name = name; 
                servicefeeC1.weekenddate = todate;
                servicefeeC1.weeknumber = rpt_weeknum;
                servicefeeC1.percentage = advertisement;
                servicefeeC1.amount_due = myadvfee;
                servicefeeC1.cash_minus_tax = mycashin + mycashout;
                servicefeeC1.cash_minus_tax_in = mycashin;
                servicefeeC1.cash_minus_tax_out = mycashout;
                servicefeeC1.last_sync = lastsync;
                servicefeeC1.type = "Advertising";
                doctype = "1";

                docyear = todate.Year.ToString().Substring(3, 1);
                docfranchise = acct.PadLeft(4, '0');
                docweek = rpt_weeknum.ToString().Trim().PadLeft(2, '0');

                servicefeeC1.doc_num = docyear + docweek + docfranchise + doctype;

                db.Dash_LD_servicefeeCorp.Add(servicefeeC1);

                db.SaveChanges();
            }

            Dash_LD_servicefeeCorp servicefeeC2 = new Dash_LD_servicefeeCorp();

            servicefeeC2.franchise_acct = Franchise_Account_Prefix + acct.PadLeft(4, '0');
            servicefeeC2.franchise_name = name; 
            servicefeeC2.weekenddate = todate;
            servicefeeC2.weeknumber = rpt_weeknum;
            servicefeeC2.percentage = royalty_in;
            servicefeeC2.amount_due = myservfee + myooafee;
            servicefeeC2.cash_minus_tax = mycashin + mycashout;
            servicefeeC2.cash_minus_tax_in = mycashin;
            servicefeeC2.cash_minus_tax_out = mycashout;
            servicefeeC2.last_sync = lastsync;
            servicefeeC2.type = "Royalty";
            doctype = "0";

            docyear = todate.Year.ToString().Substring(3, 1);
            docfranchise = acct.PadLeft(4, '0');
            docweek = rpt_weeknum.ToString().Trim().PadLeft(2, '0');

            servicefeeC2.doc_num = docyear + docweek + docfranchise + doctype;

            db.Dash_LD_servicefeeCorp.Add(servicefeeC2);

            db.SaveChanges();
        }

        public static decimal getservicein(int comp_id)
        {
            LawnDoctorTransactionDBEntities db = new LawnDoctorTransactionDBEntities();

            decimal myrtnval = 0;
            decimal actamounts = 0;

            var tmpcurAmtIn = datafees.AsEnumerable().Where(w => w["comp_id"].ToInt() == comp_id && w["transcode"].ToString().Trim() == "P"
                && w["revcode"].ToString().Trim() == "PAY");

            foreach (var item in tmpcurAmtIn)
            {

                string zippy;
                if (item.Field<string>("zipcode").Trim().Length > 5)
                {
                    zippy = item.Field<string>("zipcode").Trim().Substring(0, 5);
                }
                else
                {
                    zippy = item.Field<string>("zipcode").Trim();
                }

                byte dbid = item.Field<byte>("sqldbid");
                var cntr = db.zipcodes.Where(w => w.SQLDBID == dbid && w.zip.Trim() == zippy).FirstOrDefault();
                decimal myamt = 0;

                if (cntr != null)
                {
                    myamt = item.Field<decimal>("actamount");
                    actamounts = actamounts + Math.Abs(myamt);
                }
            }

            decimal taxamt = 0;
            var tmpcurTaxIn = datafees.AsEnumerable().Where(w => w["comp_id"].ToInt() == comp_id && w["billtype"].ToString().Trim() != "C");

            foreach (var item in tmpcurTaxIn)
            {
                
                string zippy;
                if (item.Field<string>("zipcode").Trim().Length > 5)
                {
                    zippy = item.Field<string>("zipcode").Trim().Substring(0, 5);
                }
                else
                {
                    zippy = item.Field<string>("zipcode").Trim();
                }
                byte id = item.Field<byte>("sqldbid");
                var cntr = db.zipcodes.Where(w => w.SQLDBID == id && w.zip.Trim() == zippy).FirstOrDefault();
                decimal myamt = 0;

                if (cntr != null)
                {
                    myamt = item.Field<decimal>("taxamt");
                    taxamt = taxamt + Math.Abs(myamt);
                }
            }

            string txt = "IN";
            decimal mycredits = getcredits(comp_id, txt);

            decimal thispay = Math.Round(actamounts, 2, MidpointRounding.AwayFromZero);
            decimal thistax = Math.Round(taxamt, 2, MidpointRounding.AwayFromZero);

            myrtnval = (thispay - mycredits - thistax);


            return myrtnval;
        }

        public static decimal getcredits(int comp_id, string txt)
        {
            LawnDoctorTransactionDBEntities db = new LawnDoctorTransactionDBEntities();

            decimal credamt = 0;

            var tmpcred = datafees.AsEnumerable().Where(w => w["comp_id"].ToInt() == comp_id && w.Field<string>("transcode").Trim() == "A");
            if (txt == "")  // no txt parameter passed for In/OUT
            {
                foreach (var item in tmpcred)
                {
                    var code = item.Field<string>("adjcode").Trim();
                    var adjcodeval = curCredCode.Where(w => w.adjcode.Trim() == code).FirstOrDefault();
                    if (adjcodeval != null)
                    {
                        if (adjcodeval.adjtype.Trim() == "C")
                        {
                            credamt = credamt + Math.Abs(item.Field<decimal>("actamount"));
                        }
                        else
                        {
                            credamt = credamt + Math.Abs(item.Field<decimal>("actamount")) * -1;
                        }
                    }

                }
            }
            else // service fee in / out
            {
                if (txt == "IN")
                {
                    //var tmpcurAdjust = datafees.AsEnumerable().Where(w => w["comp_id"].ToInt() == comp_id && w["transcode"].ToString().Trim() == "A");

                    credamt = 0;

                    foreach (var item in tmpcred)
                    {
                        string zippy;
                        if (item.Field<string>("zipcode").Trim().Length > 5)
                        {
                            zippy = item.Field<string>("zipcode").Trim().Substring(0, 5);
                        }
                        else
                        {
                            zippy = item.Field<string>("zipcode").Trim();
                        }
                        byte id = item.Field<byte>("sqldbid");
                        var cntr = db.zipcodes.Where(w => w.SQLDBID == id && w.zip.Trim() == zippy).FirstOrDefault();

                        if (cntr != null)
                        {
                            var code = item.Field<string>("adjcode").Trim();
                            var adjcodeval = curCredCode.Where(w => w.adjcode.Trim() == code).FirstOrDefault();
                            if (adjcodeval != null)
                            {
                                if (adjcodeval.adjtype == "C")
                                {
                                    credamt = credamt + Math.Abs(item.Field<decimal>("actamount"));
                                }
                                else
                                {
                                    credamt = credamt + Math.Abs(item.Field<decimal>("actamount")) * -1;
                                }
                            }
                        }
                    }
                }
                else // txt = OUT
                {
                    //var tmpcurAdjust = datafees.AsEnumerable().Where(w => w["comp_id"].ToInt() == comp_id && w["transcode"].ToString().Trim() == "A");

                    credamt = 0;

                    foreach (var item in tmpcred)
                    {
                        string zippy;
                        if (item.Field<string>("zipcode").Trim().Length > 5)
                        {
                            zippy = item.Field<string>("zipcode").Trim().Substring(0, 5);
                        }
                        else
                        {
                            zippy = item.Field<string>("zipcode").Trim();
                        }
                        byte id = item.Field<byte>("sqldbid");
                        var cntr = db.zipcodes.Where(w => w.SQLDBID == id && w.zip.Trim() == zippy).FirstOrDefault();

                        if (cntr == null)
                        {
                            var code = item.Field<string>("adjcode").Trim();
                            var adjcodeval = curCredCode.Where(w => w.adjcode.Trim() == code).FirstOrDefault();
                            if (adjcodeval != null)
                            {
                                if (adjcodeval.adjtype.Trim() == "C")
                                {
                                    credamt = credamt + Math.Abs(item.Field<decimal>("actamount"));
                                }
                                else
                                {
                                    credamt = credamt + Math.Abs(item.Field<decimal>("actamount")) * -1;
                                }
                            }
                        }
                    }
                }
            }

            return credamt;
        }

        public static decimal getserviceout(int comp_id)
        {
            LawnDoctorTransactionDBEntities db = new LawnDoctorTransactionDBEntities();
            decimal myrtnval = 0;
            decimal actamounts = 0;

            var tmpcurAmtOut = datafees.AsEnumerable().Where(w => w["comp_id"].ToInt() == comp_id && w["transcode"].ToString().Trim() == "P"
                && w["revcode"].ToString().Trim() == "PAY").ToList();

            foreach (var item in tmpcurAmtOut)
            {

                string zippy;
                if (item.Field<string>("zipcode").Trim().Length > 5)
                {
                    zippy = item.Field<string>("zipcode").Trim().Substring(0, 5);
                }
                else
                {
                    zippy = item.Field<string>("zipcode").Trim();
                }
                byte id = item.Field<byte>("sqldbid");
                var cntr = db.zipcodes.Where(w => w.SQLDBID == id && w.zip.Trim() == zippy).FirstOrDefault();
                decimal myamt = 0;

                if (cntr == null)
                {
                    myamt = item.Field<decimal>("actamount");
                    actamounts = actamounts + Math.Abs(myamt);
                }
            }

            decimal taxamt = 0;
            var tmpcurTaxOut = datafees.AsEnumerable().Where(w => w["comp_id"].ToInt() == comp_id && w.Field<string>("billtype").Trim() != "C");

            foreach (var item in tmpcurTaxOut)
            {

                string zippy;
                if (item.Field<string>("zipcode").Trim().Length > 5)
                {
                    zippy = item.Field<string>("zipcode").Trim().Substring(0, 5);
                }
                else
                {
                    zippy = item.Field<string>("zipcode").Trim();
                }
                byte id = item.Field<byte>("sqldbid");
                var cntr = db.zipcodes.Where(w => w.SQLDBID == id && w.zip.Trim() == zippy).FirstOrDefault();
                decimal myamt = 0;

                if (cntr == null)
                {
                    myamt = item.Field<decimal>("taxamt");
                    taxamt = taxamt + Math.Abs(myamt);
                }
            }

            string txt = "OUT";
            decimal mycredits = getcredits(comp_id, txt);

            decimal thispay = Math.Round(actamounts, 2, MidpointRounding.AwayFromZero);
            decimal thistax = Math.Round(taxamt, 2,  MidpointRounding.AwayFromZero);

            myrtnval = (thispay - mycredits - thistax);


            return myrtnval;

        }

        public static List<curcredcodecls> getcreditacctlist(int sqldbid)
        {
            LawnDoctorTransactionDBEntities db = new LawnDoctorTransactionDBEntities();

            var tmpcurAcctList = db.accounts.Where(w => w.cash_acct == true && w.sqldbid == sqldbid).ToList();

            List<curcredcodecls> onecode = new List<curcredcodecls>();

            foreach (var item in tmpcurAcctList)
            {
                var creditcodes = db.adjcodes.Where(w => w.credacct == item.account1 && w.SQLDBID == sqldbid).Select(s => s.adjcode1).ToList();
                if (creditcodes != null)
                {
                    foreach (var code in creditcodes)
                    {

                        curcredcodecls mycodes = new curcredcodecls();
                        mycodes.adjcode = code;
                        mycodes.adjtype = "C";
                        onecode.Add(mycodes);
                    }
                }

                var debitcodes = db.adjcodes.Where(w => w.debacct == item.account1 && w.SQLDBID == sqldbid).Select(s => s.adjcode1).ToList();
                foreach (var code in debitcodes)
                {
                    curcredcodecls mycodes = new curcredcodecls();
                    mycodes.adjcode = code;
                    mycodes.adjtype = "D";
                    onecode.Add(mycodes);
                }

            }
            return onecode;
        }

        public static List<byte> checkbudgettype()
        {
            // * lets check to make sure that all of the selected program/service codes have a budgetid

            LawnDoctorTransactionDBEntities db = new LawnDoctorTransactionDBEntities();
           

            List<byte> missingbudget = db.prgmcds.Where(w => (w.budgetid ?? 0) == 0 && w.SQLDBID != 255).Select(s => s.SQLDBID).Distinct().ToList();

            return missingbudget;
        }

        public static bool checkforoutofarea(int sqldbid)
        {
            // * lets check to see if charged out of area
            LawnDoctorTransactionDBEntities db = new LawnDoctorTransactionDBEntities();

            int outofarea = db.franchisefees.Where(w => w.sqldbid == sqldbid && w.chargeoutofarea == true).Select(s => s.sqldbid).FirstOrDefault();

            bool hasOutOfArea = false;

            if (outofarea != 0)
            {
                hasOutOfArea = true;
            }
            return hasOutOfArea;
        }

        public static int getweeknum()
        {
            LawnDoctorTransactionDBEntities db = new LawnDoctorTransactionDBEntities();
            DateTime lastweek = DateTime.Today.AddDays(-7);
            int week_id = db.Dash_LD_WeekNumbers.Where(w => lastweek >= w.fromdate && lastweek <= w.todate).Select(s => s.week_id).First();
            return week_id;
        }

        public static DateTime getFdatefromweeknum(int week, int year)
        {
            LawnDoctorTransactionDBEntities db = new LawnDoctorTransactionDBEntities();
            DateTime fromdate;
            fromdate = Convert.ToDateTime(db.Dash_LD_WeekNumbers.Where(w => w.year == year && w.weeknumber == week).Select(s => s.fromdate));
            return fromdate;
        }

        public static DateTime getTdatefromweeknum(int week, int year)
        {
            LawnDoctorTransactionDBEntities db = new LawnDoctorTransactionDBEntities();
            DateTime todate;
            todate = Convert.ToDateTime(db.Dash_LD_WeekNumbers.Where(w => w.year == year && w.weeknumber == week).Select(s => s.todate));
            return todate;
        }

        public static void fillweeknumbertable()
        {
            LawnDoctorTransactionDBEntities db = new LawnDoctorTransactionDBEntities();
            List<Dash_LD_WeekNumbers> addweek = new List<Dash_LD_WeekNumbers>();

            int tiyear = DateTime.Today.Year;
            int checkexist = db.Dash_LD_WeekNumbers.Where(w => w.year == tiyear).Count();

            if (checkexist == 0)  // need to add week numbers for this year to Dash_LD_weeknumber table
            {
                //** deal with spec, first week starts with jan,1
                //** work week described as monday through sunday
                //** first work week always starts Jan 1 and ends on first sunday, UNLESS there are 3 or less days in the week, If so, will be 1st through the 2nd sunday 
                //**    week 1 could exceed 7 days.  
                //** 2008 first week was 13 days long
                //** deal with end of year.  some years have 53 weeks and the report can not wrap into next calendar year. 12/31 is always 
                //** the last day of the last week of the year
                //** the last week (52) could also exceed 7 days

                // ???? int myfd = DOW(DATE(tiYear, 1, 1), 1);

                //* 1 = sunday
                //* 2 = monday
                //* 3 = tuesday
                //* 4 = wednesday
                //* 5 = thursday
                //* 6 = friday
                //* 7 = saturday
                //* 0 = sunday && same as setting to 1

                int myfd;
                int myoffset;
                DateTime myfirstweek = DateTime.Today;
                DateTime myfirstday;
                //m.myoffset = ICASE(m.myfd=1,7,m.myfd=2,6,m.myfd=3,5,m.myfd=4,4,m.myfd=5,3,m.myfd=6,9,m.myfd=7,8,m.myfd=0,1)
                //m.myfirstweek = DATE(tiyear,1,1)+m.myoffset
                //m.myfirstday = DATE(tiYear,1,1)

                int myld;
                int myendoffset;
                DateTime mylastweek;
                DateTime mylastday;
                //m.myld = DOW(DATE(tiYear,12,31),1)
                //m.myendoffset = ICASE(m.myld=1,6,m.myld=2,0,m.myld=3,1,m.myld=4,2,m.myld=5,3,m.myld=6,4,m.myld=7,5,m.myld=0,6)
                //m.mylastweek = DATE(tiYear,12,31)-m.myendoffset
                //m.mylastday = DATE(tiYear,12,31)

                //        CASE tiWeek = 52
                //            IF WEEK(DATE(tiYear,12,31),3,2) = 53
                //                m.myfrom = m.myfirstweek+((50)*7) + 1
                //                m.myto = m.mylastday				
                //**				m.myto = m.myfrom+6		jjz 09/29/12  changed so that week 52 always ends on 12/31
                //            ELSE
                //                m.myfrom = m.myfirstweek+((50)*7) + 1			
                //                m.myto = m.mylastday			
                //            ENDIF 
                //        CASE tiWeek = 53
                //                m.myfrom = m.mylastweek
                //                m.myto = m.mylastday		
                //        OTHERWISE
                //            m.myfrom = m.myfirstweek+((tiWeek-2)*7) + 1
                //            m.myto = m.myfrom+6

                for (int week = 1; week < 53; week++)
                {

                    Dash_LD_WeekNumbers oneweek = new Dash_LD_WeekNumbers();

                    myoffset = 0;
                    DateTime myfrom = DateTime.Today;
                    DateTime myto = DateTime.Today;

                    if (week == 1)
                    {
                        string fdate = "01/01/" + DateTime.Today.Year.ToString().Trim();
                        myfirstday = Convert.ToDateTime(fdate);
                        myfd = (int)(Convert.ToDateTime(fdate).DayOfWeek) + 1;
                        switch (myfd)
                        {
                            //m.myoffset = ICASE(m.myfd=1,7,m.myfd=2,6,m.myfd=3,5,m.myfd=4,4,m.myfd=5,3,m.myfd=6,9,m.myfd=7,8,m.myfd=0,1)
                            case 1:
                                myoffset = 7;
                                break;
                            case 2:
                                myoffset = 6;
                                break;
                            case 3:
                                myoffset = 5;
                                break;
                            case 4:
                                myoffset = 4;
                                break;
                            case 5:
                                myoffset = 3;
                                break;
                            case 6:
                                myoffset = 9;
                                break;
                            case 7:
                                myoffset = 8;
                                break;
                            case 0:
                                myoffset = 1;
                                break;
                        }
                        //m.myfrom = m.myfirstday
                        //m.myto = m.myfirstday +  m.myoffset 
                        myfrom = myfirstday;
                        myto = myfirstday.AddDays(myoffset);
                        myfirstweek = Convert.ToDateTime(fdate).AddDays(myoffset);
                    }

                    if (week == 52)
                    {
                        myendoffset = 0;

                        string ldate = "12/31/" + DateTime.Today.Year.ToString().Trim();
                        mylastday = Convert.ToDateTime(ldate);
                        myld = (int)(Convert.ToDateTime(ldate).DayOfWeek) + 1;
                        switch (myld)
                        {
                            case 1:
                                myendoffset = 7;
                                break;
                            case 2:
                                myendoffset = 6;
                                break;
                            case 3:
                                myendoffset = 1;
                                break;
                            case 4:
                                myendoffset = 2;
                                break;
                            case 5:
                                myendoffset = 3;
                                break;
                            case 6:
                                myendoffset = 4;
                                break;
                            case 7:
                                myendoffset = 5;
                                break;
                            case 0:
                                myendoffset = 6;
                                break;
                        }


                        //            IF WEEK(DATE(tiYear,12,31),3,2) = 53
                        //                m.myfrom = m.myfirstweek+((50)*7) + 1
                        //                m.myto = m.mylastday				
                        //**				m.myto = m.myfrom+6		jjz 09/29/12  changed so that week 52 always ends on 12/31
                        //            ELSE
                        //                m.myfrom = m.myfirstweek+((50)*7) + 1			
                        //                m.myto = m.mylastday			
                        //            ENDIF 

                        int addmyfrom = ((50) * 7) + 1;
                        myfrom = myfirstweek.AddDays(addmyfrom);
                        myto = mylastday;
                        mylastweek = Convert.ToDateTime(ldate).AddDays(myendoffset);

                    }
                    if (week > 1 && week < 52)
                    {
                        //m.myfrom = m.myfirstweek+((tiWeek-2)*7) + 1
                        //m.myto = m.myfrom+6

                        int addmyfrom = ((week - 2) * 7) + 1;
                        myfrom = myfirstweek.AddDays(addmyfrom);
                        myto = myfrom.AddDays(6);
                    }

                    oneweek.weeknumber = week;
                    oneweek.year = tiyear;
                    oneweek.fromdate = myfrom;
                    oneweek.todate = myto;
                    //addweek.Add(oneweek);
                    db.Dash_LD_WeekNumbers.Add(oneweek);
                }


                db.SaveChanges();

            }
        }
    }

    public class curcredcodecls
    {
        public string adjcode { get; set; }
        public string adjtype { get; set; }

    }

    public class servicefeecls
    {
        //public int dash_ld_servfee_id { get; set; }
        public int sqldbid { get; set; }
        public int comp_id { get; set; }
        public string branch_no { get; set; }
        public string franchise { get; set; }
        public int weeknumber { get; set; }
        public DateTime weekstartdate { get; set; }
        public DateTime weekenddate { get; set; }
        public decimal cash_minus_tax { get; set; }
        public decimal cash_minus_tax_in { get; set; }
        public decimal cash_minus_tax_out { get; set; }
        public decimal service_fee { get; set; }
        public decimal service_fee_in { get; set; }
        public decimal service_fee_out { get; set; }
        public decimal advfee { get; set; }
        public string type { get; set; }
        public decimal percentage { get; set; }
        public string doc_num { get; set; }
        public DateTime last_sync { get; set; }
        public decimal amount_due { get; set; }
        public bool showinout { get; set; }
    }

    public class servicefeeCorpcls
    {
        public string franchise_acct { get; set; }
        public string franchise_name { get; set; }
        public int weeknumber { get; set; }
        public DateTime weekenddate { get; set; }
        public decimal cash_minus_tax { get; set; }
        public string type { get; set; }
        public decimal amount_due { get; set; }
        public decimal percentage { get; set; }
        public string doc_num { get; set; }
        public DateTime last_sync { get; set; }
        public decimal cash_minus_tax_in { get; set; }
        public decimal cash_minus_tax_out { get; set; }

    }

    public class CashCompcls
    {
        public string franchise_acct { get; set; }
        public string franchise_name { get; set; }
        public int weeknumber { get; set; }
        public DateTime weekenddate { get; set; }
        public int cust_no { get; set; }
        public string last_name { get; set; }
        public string first_name { get; set; }
        public DateTime histdate { get; set; }
        public string check_no { get; set; }
        public decimal amount { get; set; }
        public decimal in_area { get; set; }
        public decimal out_of_area { get; set; }
        public decimal total_tax { get; set; }
        public decimal cash_minus_tax { get; set; }
    }

    public class WEBScls
    {
        public string franchise_acct { get; set; }
        public string franchise_name { get; set; }
        public int weeknumber { get; set; }
        public DateTime weekenddate { get; set; }
        public string type { get; set; }
        public int lead { get; set; }
        public int sales_no { get; set; }
        public decimal sales_dollars { get; set; }
        public int cancel_no { get; set; }
        public decimal cancel_dollars { get; set; }
        public int non_renew_sales_no { get; set; }
        public decimal non_renew_sales_dollars { get; set; }
        public int renew_sales_no { get; set; }
        public decimal renew_sales_dollars { get; set; }
        public int non_renew_cancel_no { get; set; }
        public decimal non_renew_cancel_dollars { get; set; }
        public int renew_cancel_no { get; set; }
        public decimal renew_cancel_dollars { get; set; }
        public int change_in_CIF { get; set; }
        public decimal change_in_BIF { get; set; }
        public int CIF_cancel { get; set; }
        public decimal BIF_cancel { get; set; }
    }
    public class LDWebsSummary
    {
        public string AcctNo { get; set; }

        public string Franchise { get; set; }

        public int weeknumber { get; set; }

        public string weekenddate { get; set; }

        public string budgettype { get; set; }

        public int CCsalesnumber { get; set; }

        public Decimal CCsalesdollar { get; set; }

        public int CCcancelnumber { get; set; }

        public Decimal CCcanceldollars { get; set; }

        public int YTD_cc_sales_no { get; set; }

        public Decimal YTD_cc_Sales_dollar { get; set; }

        public int YTD_cc_cancel_no { get; set; }

        public Decimal YTD_cc_cancel_dollar { get; set; }

        public int CC_Active_Count { get; set; }

        public Decimal cc_active_dollar { get; set; }

        public int NS_Cif { get; set; }

        public Decimal NS_BIF { get; set; }

        public Decimal NS_sqft { get; set; }

        public int CIFCancel { get; set; }

        public Decimal BIFCancel { get; set; }

        public int YTD_NS_CIF { get; set; }

        public Decimal YTD_NS_BIF { get; set; }

        public Decimal YTD_NS_sqft { get; set; }

        public int YTD_CIF_Cancel { get; set; }

        public Decimal YTD_BIF_Cancel { get; set; }

        public int Active_Program_CIF { get; set; }

        public Decimal Active_Program_BIF { get; set; }

        public Decimal active_program_sqft { get; set; }
    }
}

