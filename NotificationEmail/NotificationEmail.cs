﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Principal;
using System.Net.Mail;
using System.Text;
using System.Diagnostics;

namespace NotificationEmail
{
    public static class NotificationEmail
    {
        public static void ProgressNotification(string message)
        {
            return;
            string _source = "LD Weekly report - Progress";
            string _log = "Application";

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("LD Weekly Reports Progress : " + message);

            if (!EventLog.SourceExists(_source))
            {
                EventLog.CreateEventSource(_source, _log);
            }

            EventLog.WriteEntry(_source, sb.ToString(), EventLogEntryType.Information, 1);
        }
        public static void MissingInformationNotification(string directory, string dbname, string tablename, List<string> missing, string subject)
        {
            string _source = "LD Weekly report";
            string _log = "Application";

            // create email with missing information 

            string toEmail = "lawndoctorreports@realgreen.com";
            string fromEmail = Properties.Settings.Default.FromEmail;
            string mailSubject = subject;
            string body = "";

            foreach (var item in missing)
            {
                if (!string.IsNullOrWhiteSpace(item))
                {
                    body += "\r\n\r\n" + item;
                }
            }

            SmtpClient c = new SmtpClient();
            MailMessage m = new MailMessage(fromEmail, toEmail, mailSubject, body);
            m.To.Add(new MailAddress("lawndoctorreports@realgreen.com"));
            m.IsBodyHtml = false;
            try
            {
                c.Send(m);
            }
            catch (Exception ex)
            {   
                StringBuilder sb = new StringBuilder();
                sb.AppendLine(ex.InnerException.Message.ToString());
                sb.AppendLine(ex.StackTrace.ToString());
                if (!EventLog.SourceExists(_source))
                {
                    EventLog.CreateEventSource(_source, _log);
                }
                EventLog.WriteEntry(_source, sb.ToString(), EventLogEntryType.Error, 1);
            }
        }

        public static void DataToOrder(string directory, string dbname, string tablename, List<string> missing, string batchnum)
        {
            string _source = "LD Weekly report";
            string _log = "Application";
            // create email with missing information when trying to process Strategic America Orders

            string errorMessage = "";
            string line;
            string sqlcmd;

            foreach (var item in missing)
            {
                line = item.Trim();
                errorMessage += line + "\n\n";
            }

            sqlcmd = "select * from dashemail with (nolock) where type = 'OrderData'";
            string connStr = Properties.Settings.Default.EmailConnectionString;
            DataTable emails = CommonTools.CommonTools.SQL_GetDT(sqlcmd, connStr);

            int validemails = 0;
            string toEmail = "";
            foreach (DataRow mail in emails.AsEnumerable())
            {
                if (validemails != 0)
                {
                    toEmail = toEmail += "," + mail.Field<string>("email").Trim();
                }
                else
                {
                    toEmail = mail.Field<string>("email").Trim();
                }
                validemails++;
            }

            if (validemails != 0)
            {
                //Microsoft.Win32.Registry.LocalMachine.GetValue(
                string fromEmail = Properties.Settings.Default.FromEmail;
                string mailSubject = "Exmark Data needs to be ordered for Batch: " + batchnum.Trim();
                try
                {
                    EmailUtil.EmailUtil.SendEmail(fromEmail, toEmail, mailSubject, errorMessage, false);
                }
                catch (Exception ex)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine(ex.InnerException.Message.ToString());
                    sb.AppendLine(ex.StackTrace.ToString());
                    if (!EventLog.SourceExists(_source))
                    {
                        EventLog.CreateEventSource(_source, _log);
                    }
                    EventLog.WriteEntry(_source, sb.ToString(), EventLogEntryType.Error, 1);
                }

            }
        }

        public static void AG_Data(string directory, string dbname, string tablename, List<string> missing, string batchnum)
        {
            string _source = "LD Weekly report";
            string _log = "Application";
            // create email with missing information when trying to process Strategic America Orders

            string errorMessage = "";
            string line;
            string sqlcmd;

            foreach (var item in missing)
            {
                line = item.Trim();
                errorMessage += line + "\n\n";
            }

            sqlcmd = "select * from dashemail with (nolock) where type = 'OrderData'";
            string connStr = Properties.Settings.Default.EmailConnectionString;
            DataTable emails = CommonTools.CommonTools.SQL_GetDT(sqlcmd, connStr);

            int validemails = 0;
            string toEmail = "";
            foreach (DataRow mail in emails.AsEnumerable())
            {
                if (validemails != 0)
                {
                    toEmail = toEmail += "," + mail.Field<string>("email").Trim();
                }
                else
                {
                    toEmail = mail.Field<string>("email").Trim();
                }
                validemails++;
            }

            if (validemails != 0)
            {
                //Microsoft.Win32.Registry.LocalMachine.GetValue(
                string fromEmail = Properties.Settings.Default.FromEmail;
                string mailSubject = "Exmark AGFARM Data needed from Strategic America for Batch: " + batchnum.Trim();

                try
                {
                    EmailUtil.EmailUtil.SendEmail(fromEmail, toEmail, mailSubject, errorMessage, false);
                }
                catch (Exception ex)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine(ex.InnerException.Message.ToString());
                    sb.AppendLine(ex.StackTrace.ToString());
                    if (!EventLog.SourceExists(_source))
                    {
                        EventLog.CreateEventSource(_source, _log);
                    }
                    EventLog.WriteEntry(_source, sb.ToString(), EventLogEntryType.Error, 1);
                }
            }
        }

        public static void EmailReport(string filepath,string subject,string to)
        {
            string _source = "LD Weekly report";
            string _log = "Application";
            // create email with Service Fee report attached

            string connStr = Properties.Settings.Default.EmailConnectionString;

            string toEmail = to;
            string fromEmail = Properties.Settings.Default.FromEmail;
            string mailSubject = subject;
            string body = "";

            SmtpClient c = new SmtpClient();
            MailMessage m = new MailMessage(fromEmail, toEmail, mailSubject, body);
            m.To.Add(new MailAddress("lawndoctorreports@realgreen.com"));
            m.IsBodyHtml = false;
            m.Attachments.Add(new Attachment(filepath));
            try
            {
                c.Send(m);
                //EmailUtil.EmailUtil.SendEmail(fromEmail, toEmail, mailSubject, errorMessage, false);
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine(ex.InnerException.Message.ToString());
                sb.AppendLine(ex.StackTrace.ToString());
                if (!EventLog.SourceExists(_source))
                {
                    EventLog.CreateEventSource(_source, _log);
                }
                EventLog.WriteEntry(_source, sb.ToString(), EventLogEntryType.Error, 1);
            }
        }

        public static void TestEmail(string directory, string dbname, string tablename, string missing)
        {
            string _source = "LD Weekly report";
            string _log = "Application";
            // create email with missing information when trying to process Strategic America Orders

            string errorMessage = "This is the body of a test message";

            string connStr = Properties.Settings.Default.EmailConnectionString;

            string toEmail = "lawndoctorreports@realgreen.com";
            string fromEmail = Properties.Settings.Default.FromEmail;
            string mailSubject = "Test email from Exmark Data Utility";
            try
            {
                EmailUtil.EmailUtil.SendEmail(fromEmail, toEmail, mailSubject, errorMessage, false);
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine(ex.InnerException.Message.ToString());
                sb.AppendLine(ex.StackTrace.ToString());
                if (!EventLog.SourceExists(_source))
                {
                    EventLog.CreateEventSource(_source, _log);
                }
                EventLog.WriteEntry(_source, sb.ToString(), EventLogEntryType.Error, 1);
            }

        }

        public static void ApplicationError(Exception mailex, string appName)
        {
            ApplicationError(mailex, appName, string.Empty);
        }

        public static void ApplicationError(Exception mailEx, string appName, string note)
        {
            string _source = "LD Weekly report";
            string _log = "Application";
            try
            {
                string toEmail = "lawndoctorreports@realgreen.com";
                string fromEmail = Properties.Settings.Default.FromEmail;
                string mailSubject = appName + " Lawn Doctor Reports Error - " + WindowsIdentity.GetCurrent().Name + " : " + Environment.MachineName;

                string body = DateTime.Now.ToString() + "\r\n" + mailEx.ToString();
                if (mailEx.InnerException != null)
                {
                    body += "\r\n" + mailEx.InnerException.ToString();
                }

                if (!string.IsNullOrWhiteSpace(note))
                {
                    body += "\r\n\r\n" + note;
                }

                SmtpClient c = new SmtpClient();
                MailMessage m = new MailMessage(fromEmail, toEmail, mailSubject, body);
                //m.To.Add(new MailAddress("skatz@realgreen.com"));
                m.IsBodyHtml = false;
                c.Send(m);
                //EmailUtil.EmailUtil.SendEmail(fromEmail, toEmail, mailSubject, body, false);
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine(ex.InnerException.Message.ToString());
                sb.AppendLine(ex.StackTrace.ToString());
                if (!EventLog.SourceExists(_source))
                {
                    EventLog.CreateEventSource(_source, _log);
                }
                EventLog.WriteEntry(_source, sb.ToString(), EventLogEntryType.Error, 1);
            }
        }

        private static void SendToClientNotification(int days, string email)
        {
            if (email.Trim() == "" || email.Contains("@") == false)
            {
                return;
            }
            string fromEmail = Properties.Settings.Default.FromEmail;
            string toEmail = email.Trim();
            string mailSubject = "CAW Sync Notification";
            string errorMessage = "Greetings Real Green Customer, \n\n" +
                            "It has come to our attention that there may be a problem with your Customer Assistant Website synchronization process. \n\n" +
                            "Please contact Real Green Systems technical support at your earliest convenience so that we may assist you in resolving this issue.\n\n" +
                            "Thank you for your time. \n" + "Have a nice day.\n\n" +
                            "Real Green Technical Support Team \n" + "1(800)422-7478\n" +
                            "http://customer.realgreen.com";
            // EmailUtils.SendEmail(fromEmail, toEmail, mailSubject, errorMessage, false);
        }
    }
}
