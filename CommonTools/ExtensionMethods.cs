﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Data;
using System.Reflection;
//using System.Data.Objects;
using System.Drawing;
using System.Drawing.Imaging;
using System.ComponentModel;
//using System.Data.Objects.DataClasses;
//using System.Data.Metadata.Edm;




public static class ExtensionMethods
{
    public static IList<T> Clone<T>(this IList<T> listToClone) where T : ICloneable
    {
        return listToClone.Select(item => (T)item.Clone()).ToList();
    }


    public static void ToCSV(this DataTable dt, string strFilePath)
    {
        StreamWriter sw = new StreamWriter(strFilePath, false);
        int iColCount = dt.Columns.Count;
        for (int i = 0; i <= iColCount - 1; i++)
        {
            sw.Write(dt.Columns[i]);
            if (i < iColCount - 1)
            {
                sw.Write(",");
            }
        }
        sw.Write("\r\n");
        foreach (DataRow dr in dt.Rows)
        {
            for (int i = 0; i <= iColCount - 1; i++)
            {
                if (!Convert.IsDBNull(dr[i]))
                {
                    sw.Write(string.Format("\"{0}\"", dr[i].ToString()));
                }
                if (i < iColCount - 1)
                {
                    sw.Write(",");
                }
            }
            sw.Write("\r\n");
        }
        sw.Close();
    }

    public static string ToTitleCase(this string mText)
    {
        if (mText == null) return mText;

        System.Globalization.CultureInfo cultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture;
        System.Globalization.TextInfo textInfo = cultureInfo.TextInfo;

        // TextInfo.ToTitleCase only operates on the string if is all lower case, otherwise it returns the string unchanged.
        return textInfo.ToTitleCase(mText.ToLower());
    }

    public static bool IsLike(this string s, string wildcardPattern)
    {
        /// <summary>
        /// Indicates whether the current string matches the supplied wildcard pattern.  Behaves the same
        /// as VB's "Like" Operator.
        /// </summary>
        /// <param name="s">The string instance where the extension method is called</param>
        /// <param name="wildcardPattern">The wildcard pattern to match.  Syntax matches VB's Like operator.</param>
        /// <returns>true if the string matches the supplied pattern, false otherwise.</returns>
        /// <remarks>See http://msdn.microsoft.com/en-us/library/swf8kaxw(v=VS.100).aspx</remarks>
        /// 
        if (s == null || String.IsNullOrEmpty(wildcardPattern)) return false;
        // turn into regex pattern, and match the whole string with ^$
        var regexPattern = "^" + Regex.Escape(wildcardPattern) + "$";

        // add support for ?, #, *, [], and [!]
        regexPattern = regexPattern.Replace(@"\[!", "[^")
                                   .Replace(@"\[", "[")
                                   .Replace(@"\]", "]")
                                   .Replace(@"\?", ".")
                                   .Replace(@"\*", ".*")
                                   .Replace(@"\#", @"\d");

        var result = false;
        try
        {
            result = Regex.IsMatch(s, regexPattern);
        }
        catch (ArgumentException ex)
        {
            throw new ArgumentException(String.Format("Invalid pattern: {0}", wildcardPattern), ex);
        }
        return result;
    }

    public static DataTable ToDataTable<T>(this IList<T> list)
    {
        PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(T));
        DataTable table = new DataTable();
        for (int i = 0; i < props.Count; i++)
        {
            PropertyDescriptor prop = props[i];
            table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
        }
        object[] values = new object[props.Count];
        foreach (T item in list)
        {
            for (int i = 0; i < values.Length; i++)
                values[i] = props[i].GetValue(item) ?? DBNull.Value;
            table.Rows.Add(values);
        }
        return table;
    }

    public static DataTable AsDataTable<T>(this IEnumerable<T> enumberable)
    {
        DataTable table = new DataTable("Generated");

        T first = enumberable.FirstOrDefault();
        if (first == null)
            return table;

        PropertyInfo[] properties = first.GetType().GetProperties();

        foreach (PropertyInfo pi in properties)
        {
            Type propType = pi.PropertyType;
            if (pi.PropertyType.IsGenericType && pi.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                propType = pi.PropertyType.GetGenericArguments()[0];
            }
            else
            {
                propType = pi.PropertyType;
            }
            table.Columns.Add(pi.Name, propType);
        }
        foreach (T t in enumberable)
        {
            DataRow row = table.NewRow();
            foreach (PropertyInfo pi in properties)
                row[pi.Name] = pi.GetValue(t,null) ?? DBNull.Value;
            //row[pi.Name] = t.GetType().InvokeMember(pi.Name, BindingFlags.GetProperty, null, t, null);
            table.Rows.Add(row);
        }

        return table;
    }

    public static bool In<T>(this T source, params T[] list)
    {
        if (null == source) throw new ArgumentNullException("source");
        return list.Contains(source);
    }

    public static int ToInt(this object obj)
    {
        if (obj == null) obj = 0;
        return Convert.ToInt32(obj);
    }
    public static Image ToImage(this byte[] bArray)
    {
        Image img;
        using (MemoryStream ms = new MemoryStream(bArray))
        {
            img = Image.FromStream(ms);
        }
        return img;
    }

    public static byte[] ToByteArray(this Image img)
    {
        MemoryStream imgStream = new MemoryStream();
        img.Save(imgStream, ImageFormat.Png);
        imgStream.Close();
        byte[] byteArray = imgStream.ToArray();
        imgStream.Dispose();
        return byteArray;
    }

    public static string RemoveBlankLines(this string str)
    {
        return str.Replace("\r\n\r\n", "\r\n");
    }

    public static string RemoveEndingCR(this string str)
    {
        if (str.EndsWith("\r\n"))
        {
            str = str.Remove(str.Length - 2, 2);
            return RemoveEndingCR(str);
        }
        else
        {
            return str;
        }
    }
    public static string RemoveHTML(this string strText)
    {
        Regex RegEx = new Regex("<[^>]*>");
        return RegEx.Replace(strText, "");
    }

}