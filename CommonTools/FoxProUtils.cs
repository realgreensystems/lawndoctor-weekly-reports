﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Data.Odbc;
using System.Data.SqlClient;

namespace FoxUtil
{
    public static class FoxProUtils
    {
        public static DatabaseFactoryBase myFactory = new OleDatabaseFactory();

        public static DataSet ReadDbf(string directory, params string[] tablePaths)
        {
            //return new CsswDataSet();
            DataSet results = new DataSet();
            foreach (string path in tablePaths)
            {

                using (DbConnection connection = myFactory.CreateConnectionFromDirectory(directory))
                {
                    connection.Open();
                    string tableName = System.IO.Path.GetFileNameWithoutExtension(path);
                    string selectCommand = "SELECT * FROM [" + tableName + "]";

                    //OleDbDataAdapter adapter = new OleDbDataAdapter(selectCommand, connection);
                    DbDataAdapter adapter = FoxProUtils.CreateAdapter(selectCommand, connection);

                    adapter.TableMappings.Add("Table", System.IO.Path.GetFileNameWithoutExtension(tableName));
                    adapter.MyFill(results);
                    connection.Close();
                }
            }
            return results;
        }

        /// <summary>
        /// Preconditions:
        ///  * dbfDirectory is a file folder that exists;
        ///  * The static FoxProUtils.myFactory field must be assigned a valid factory instance.
        /// </summary>
        /// <param name="dbfDirectory"></param>
        /// <returns></returns>
        public static DbConnection CreateConnection(string dbfDirectory)
        {
            //string connectionString = "Provider=VFPOLEDB.1;Data Source=" + dbfDirectory + ";Exclusive=NO;Collate=MACHINE;OLE DB Services=0;";
            //return new OleDbConnection(connectionString);

            if (!System.IO.Directory.Exists(dbfDirectory))
            {
                throw new Exception("dbfDirectory doesn't exist. You need to pass a valid dbfDirectory to CreateConnection().");
            }
            else if(myFactory == null)
            {
                throw new Exception("FoxProUtils.myFactory is null. The static FoxProUtils.myFactory field must be assigned a valid factory instance.");
            }

            return myFactory.CreateConnectionFromDirectory(dbfDirectory);
        }

        public static DbDataAdapter CreateAdapter(string selectCommandText, DbConnection connection)
        {
            DbDataAdapter adapter = myFactory.CreateAdapter();
            adapter.SelectCommand = connection.CreateCommand();
            adapter.SelectCommand.CommandText = selectCommandText;
            adapter.SelectCommand.Connection = connection;

            return adapter;
        }

        public static void ExecuteUpdate(DbConnection connection, string updateText)
        {
            if (connection.State != ConnectionState.Open)
            {
                connection.Open();
            }

            DbCommand command = connection.CreateCommand();
            command.CommandType = CommandType.Text;
            command.CommandText = updateText;
            command.CommandTimeout = 60;
            command.MyExecuteNonQuery();
            connection.Close();
        }

        public static void MyExecuteNonQuery(this DbCommand command)
        {
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                // throw new SyncOleDbException(command, ex);
            }
        }

        public static void MyFill(this DbDataAdapter adapter, DataSet results)
        {
            try
            {
                adapter.Fill(results);
            }
            catch (Exception ex)
            {
                //throw new SyncOleDbException(adapter.SelectCommand, ex);
            }
        }

        public static DbParameter AddWithValue(this DbParameterCollection parameters, string name, object value)
        {
            //int paramIndex = parameters.Add(value);
            //DbParameter param = parameters[paramIndex];
            //param.ParameterName = name;

            //return param;

            DbParameter param = myFactory.CreateParameter(name, value);
            parameters.Add(param);
            return param;
        }

        public static SqlConnection CreateSqlConnection(string dbfDirectory)
        {
            //string connectionString = "driver=SQL Server;server=" + dbfDirectory + ";database=realgreen;Trusted_Connection=True;";
            //return new OleDbConnection(connectionString);

            SqlConnectionStringBuilder b = new SqlConnectionStringBuilder();
            b.DataSource = dbfDirectory.Trim();
            b.InitialCatalog = ""; // "cawmaster";
            b.IntegratedSecurity = true;
            string connectionStringSQLserver = b.ToString();

            return new SqlConnection(connectionStringSQLserver);
        }

        public static SqlDataAdapter CreateSqlAdapter(string selectCommandText, SqlConnection connection)
        {
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = connection.CreateCommand();
            adapter.SelectCommand.CommandText = selectCommandText;
            adapter.SelectCommand.Connection = connection;

            return adapter;
        }



    }



    public abstract class DatabaseFactoryBase
    {
        public abstract DbConnection CreateConnection(string connectionString);

        public abstract DbConnection CreateConnectionFromDirectory(string dbfDirectory);

        public abstract DbDataAdapter CreateAdapter();

        public abstract DbParameter CreateParameter(string name, object value);

        //public abstract DbConnection CreateCommand();
    }

    public class OleDatabaseFactory : DatabaseFactoryBase
    {

        public override DbConnection CreateConnection(string connectionString)
        {
            return new OleDbConnection(connectionString);
        }

        public override DbConnection CreateConnectionFromDirectory(string dbfDirectory)
        {
            string connectionString = "Provider=VFPOLEDB.1;Data Source=" + dbfDirectory + ";Exclusive=NO;Collate=MACHINE;OLE DB Services=0;";
            return new OleDbConnection(connectionString);
        }

        public override DbDataAdapter CreateAdapter()
        {
            return new OleDbDataAdapter();
        }

        public override DbParameter CreateParameter(string name, object value)
        {
            return new OleDbParameter(name, value);
        }
    }

    public class OdbcDatabaseFactory : DatabaseFactoryBase
    {

        public override DbConnection CreateConnection(string connectionString)
        {
            return new OdbcConnection(connectionString);
        }

        public override DbConnection CreateConnectionFromDirectory(string dbfDirectory)
        {
            string connectionString = "Driver={Microsoft Visual FoxPro Driver};SourceType=DBF;SourceDB=" + dbfDirectory + ";Exclusive=No; Collate=Machine;NULL=NO;DELETED=YES;BACKGROUNDFETCH=NO;";
            return new OdbcConnection(connectionString);
        }

        public override DbDataAdapter CreateAdapter()
        {
            return new OdbcDataAdapter();
        }

        public override DbParameter CreateParameter(string name, object value)
        {
            return new OdbcParameter(name, value);
        }
    }
}
