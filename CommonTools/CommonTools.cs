﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;

namespace CommonTools
{
    public static class CommonTools
    {
        public static DataTable Data_GetDT(string fileName, string table)
        {
            string connStrGetDT = "";

            if (Path.GetExtension(fileName) == ".xls")
            {
                connStrGetDT = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source=" + fileName + "; Extended Properties=\"Excel 8.0;IMEX=1;HDR=Yes\"";
            }
            else if (Path.GetExtension(fileName) == ".xlsx")
            {
                connStrGetDT = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source=" + fileName + "; Extended Properties=\"Excel 12.0;IMEX=1;HDR=Yes\"";
            }
            else if (Path.GetExtension(fileName) == ".csv")
            {
                connStrGetDT = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source=" + Path.GetDirectoryName(fileName) + "; Extended Properties=\"Text;HDR=Yes;FMT=CSVDelimited\"";
            }

            OleDbConnection oledbConn = new OleDbConnection(connStrGetDT);
            DataSet ds = new DataSet();
            try
            {
                oledbConn.Open();
                DataTable dt = new DataTable();
                dt = oledbConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string shtName = dt.Rows[0]["TABLE_NAME"].ToString();
                if (Path.GetExtension(fileName) == ".csv")
                    shtName = Path.GetFileName(fileName);
                OleDbCommand cmd = new OleDbCommand("SELECT * FROM [" + shtName + "]", oledbConn);
                OleDbDataAdapter oleda = new OleDbDataAdapter();
                oleda.SelectCommand = cmd;
                oleda.Fill(ds);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                oledbConn.Close();
            }
            return ds.Tables[0];
        }

        public static DataTable SQL_GetDT(string qStr, string cnStr)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(cnStr))
                {
                    // SqlConnection.InfoMessage += (System.Object sender, System.Object e) => DataToolBox.GETSQLMSG(sender, e);
                    SqlCommand sqlCommand = new SqlCommand(qStr, sqlConnection);
                    sqlCommand.CommandTimeout = 0;
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlCommand);

                    sqlAdapter.Fill(dt);
                    sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return dt;
        }

        public static DataSet SQL_GetDS(string qStr, string cnStr)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(cnStr))
                {
                    // SqlConnection.InfoMessage += (System.Object sender, System.Object e) => DataToolBox.GETSQLMSG(sender, e);
                    SqlCommand sqlCommand = new SqlCommand(qStr, sqlConnection);

                    SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlCommand);

                    sqlAdapter.Fill(ds);
                    sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return ds;
        }

        public static int SQL_EXEC(string qStr, string cnStr)
        {
            SqlConnection sqlConnection = new SqlConnection(cnStr);
            SqlCommand nonqueryCommand = sqlConnection.CreateCommand();
            int intResult = 0;
            // SqlConnection.InfoMessage += (System.Object sender, System.Object e) => DataToolBox.GETSQLMSG(sender, e);
            try
            {
                sqlConnection.Open();
                nonqueryCommand.CommandText = qStr;
                intResult = nonqueryCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                nonqueryCommand = null;
                sqlConnection.Close();
                sqlConnection = null;
            }

            return intResult;
        }
    }
}
