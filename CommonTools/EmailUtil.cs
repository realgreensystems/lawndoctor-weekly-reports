﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;

namespace EmailUtil
{
    public static class EmailUtil
    {
        public static void SendEmail(string from, string to, string subject, string body)
        {
            SendEmail(from, to, subject, body, true);
        }

        public static void SendEmail(string from, string to, string subject, string body, bool isHtml)
        {
            SmtpClient c = new SmtpClient();
            MailMessage m = new MailMessage(from, to, subject, body);
            m.IsBodyHtml = isHtml;
            c.Send(m);
        }
    }
}
