//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class Dash_LD_CashCompliance
    {
        public int dash_ld_cashcomp_id { get; set; }
        public int sqldbid { get; set; }
        public int comp_id { get; set; }
        public int weeknumber { get; set; }
        public System.DateTime weekenddate { get; set; }
        public int cust_no { get; set; }
        public string last_name { get; set; }
        public string first_name { get; set; }
        public Nullable<System.DateTime> histdate { get; set; }
        public string check_no { get; set; }
        public decimal amount { get; set; }
        public Nullable<decimal> in_area { get; set; }
        public Nullable<decimal> out_of_area { get; set; }
        public Nullable<decimal> total_tax { get; set; }
        public Nullable<decimal> cash_minus_tax { get; set; }
    }
}
