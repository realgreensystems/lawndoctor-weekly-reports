//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class service
    {
        public int service_id { get; set; }
        public int prog_id { get; set; }
        public string serv_code { get; set; }
        public string servstatus { get; set; }
        public Nullable<decimal> size { get; set; }
        public string disccode { get; set; }
        public Nullable<decimal> price { get; set; }
        public Nullable<decimal> discamt { get; set; }
        public Nullable<decimal> ppayamt { get; set; }
        public Nullable<decimal> totalamt { get; set; }
        public Nullable<short> round { get; set; }
        public Nullable<int> invoice_no { get; set; }
        public Nullable<System.DateTime> startdate { get; set; }
        public Nullable<int> prepayid { get; set; }
        public Nullable<decimal> taxblamt1 { get; set; }
        public Nullable<decimal> taxblamt2 { get; set; }
        public Nullable<decimal> taxblamt3 { get; set; }
        public Nullable<decimal> prgdiscamt { get; set; }
        public Nullable<decimal> tax1 { get; set; }
        public Nullable<decimal> tax2 { get; set; }
        public Nullable<decimal> tax3 { get; set; }
        public Nullable<System.DateTime> posted { get; set; }
        public Nullable<decimal> nextprice { get; set; }
        public Nullable<decimal> manhrrate { get; set; }
        public Nullable<decimal> prodvalue { get; set; }
        public Nullable<decimal> nextsize { get; set; }
        public string technote { get; set; }
        public Nullable<bool> reversed { get; set; }
        public byte SQLDBID { get; set; }
        public Nullable<decimal> checksum { get; set; }
        public Nullable<System.DateTime> datesold { get; set; }
        public string soldby1 { get; set; }
        public string soldby2 { get; set; }
        public string assoc { get; set; }
        public string custnote { get; set; }
        public Nullable<short> schedtime { get; set; }
        public Nullable<short> startafter { get; set; }
        public Nullable<short> endbefore { get; set; }
        public Nullable<bool> promised { get; set; }
    }
}
