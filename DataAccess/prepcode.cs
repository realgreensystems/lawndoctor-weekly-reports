//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class prepcode
    {
        public string prepcode1 { get; set; }
        public string prep_desc { get; set; }
        public Nullable<decimal> percent { get; set; }
        public bool installmnt { get; set; }
        public byte SQLDBID { get; set; }
        public Nullable<decimal> checksum { get; set; }
        public bool available { get; set; }
        public bool anybranch { get; set; }
    }
}
