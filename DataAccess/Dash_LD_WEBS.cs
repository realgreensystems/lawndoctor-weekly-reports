//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class Dash_LD_WEBS
    {
        public int dash_ld_webs_id { get; set; }
        public int sqldbid { get; set; }
        public int comp_id { get; set; }
        public int weeknumber { get; set; }
        public System.DateTime weekenddate { get; set; }
        public string budgettype { get; set; }
        public Nullable<int> leads { get; set; }
        public Nullable<int> changeinCIF { get; set; }
        public Nullable<decimal> changeinBIF { get; set; }
        public Nullable<int> CIFcancel { get; set; }
        public Nullable<decimal> BIFcancel { get; set; }
        public Nullable<int> CCsalesnumber { get; set; }
        public Nullable<decimal> CCsalesdollar { get; set; }
        public Nullable<int> CCcancelnumber { get; set; }
        public Nullable<decimal> CCcanceldollars { get; set; }
        public Nullable<int> CCnonrensalesnumber { get; set; }
        public Nullable<decimal> CCnonrensalesdollars { get; set; }
        public Nullable<int> CCnonrencancelnumber { get; set; }
        public Nullable<decimal> CCnonrencanceldollars { get; set; }
        public Nullable<int> CCrensalesnumber { get; set; }
        public Nullable<decimal> CCrensalesdollars { get; set; }
        public Nullable<int> CCrencancelnumber { get; set; }
        public Nullable<decimal> CCrencanceldollars { get; set; }
        public string AcctNo { get; set; }
    }
}
